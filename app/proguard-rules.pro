#一般以下情况都会不混淆：
#1.使用了自定义控件那么要保证它们不参与混淆
#2.使用了枚举要保证枚举不被混淆
#3.对第三方库中的类不进行混淆
#4.运用了反射的类也不进行混淆
#5.使用了 Gson 之类的工具要使 JavaBean 类即实体类不被混淆
#6.在引用第三方库的时候，一般会标明库的混淆规则的，建议在使用的时候就把混淆规则添加上去，免得到最后才去找
#7.有用到 WebView 的 JS 调用也需要保证写的接口方法不混淆，原因和第一条一样
#8.Parcelable 的子类和 Creator 静态成员变量不混淆，否则会产生 Android.os.BadParcelableException 异常

 #
 # 对于一些基本指令的添加
 #
 #############################################
 # 代码混淆压缩比，在0~7之间，默认为5，一般不做修改
 -optimizationpasses 5

 # 混合时不使用大小写混合，混合后的类名为小写
 -dontusemixedcaseclassnames

 # 指定不去忽略非公共库的类
 -dontskipnonpubliclibraryclasses

 # 这句话能够使我们的项目混淆后产生映射文件
 # 包含有类名->混淆后类名的映射关系
 -verbose

 # 指定不去忽略非公共库的类成员
 -dontskipnonpubliclibraryclassmembers

 # 不做预校验，preverify是proguard的四个步骤之一，Android不需要preverify，去掉这一步能够加快混淆速度。
 -dontpreverify

 # 保留Annotation不混淆
 -keepattributes *Annotation*,InnerClasses

#忽略警告
-ignorewarning

#抑制警告
-ignorewarnings


 # 避免混淆泛型
 -keepattributes Signature

 # 抛出异常时保留代码行号
 -keepattributes SourceFile,LineNumberTable

 # 指定混淆是采用的算法，后面的参数是一个过滤器
 # 这个过滤器是谷歌推荐的算法，一般不做更改
 -optimizations !code/simplification/cast,!field/*,!class/merging/*


 #############################################
 #
 # Android开发中一些需要保留的公共部分
 #
 #############################################

 # 保留我们使用的四大组件，自定义的Application等等这些类不被混淆
 # 因为这些子类都有可能被外部调用
 -keep public class * extends android.app.Activity
 -keep public class * extends android.app.Appliction
 -keep public class * extends android.app.Service
 -keep public class * extends android.content.BroadcastReceiver
 -keep public class * extends android.content.ContentProvider
 -keep public class * extends android.app.backup.BackupAgentHelper
 -keep public class * extends android.preference.Preference
 -keep public class * extends android.view.View
 -keep public class com.android.vending.licensing.ILicensingService


 # 保留support下的所有类及其内部类
 -keep class android.support.** {*;}

 # 保留继承的
 -keep public class * extends android.support.v4.**
 -keep public class * extends android.support.v7.**
 -keep public class * extends android.support.annotation.**

 # 保留R下面的资源
 -keep class **.R$* {*;}

 # 保留本地native方法不被混淆
 -keepclasseswithmembernames class * {
     native <methods>;
 }

 # 保留在Activity中的方法参数是view的方法，
 # 这样以来我们在layout中写的onClick就不会被影响
 -keepclassmembers class * extends android.app.Activity{
     public void *(android.view.View);
 }

 # 保留枚举类不被混淆
 -keepclassmembers enum * {
     public static **[] values();
     public static ** valueOf(java.lang.String);
 }

 # 保留我们自定义控件（继承自View）不被混淆
 -keep public class * extends android.view.View{
     *** get*();
     void set*(***);
     public <init>(android.content.Context);
     public <init>(android.content.Context, android.util.AttributeSet);
     public <init>(android.content.Context, android.util.AttributeSet, int);
 }

 # 保留Parcelable序列化类不被混淆
 -keep class * implements android.os.Parcelable {
     public static final android.os.Parcelable$Creator *;
 }

 # 保留Serializable序列化的类不被混淆
 -keepclassmembers class * implements java.io.Serializable {
     static final long serialVersionUID;
     private static final java.io.ObjectStreamField[] serialPersistentFields;
     !static !transient <fields>;
     !private <fields>;
     !private <methods>;
     private void writeObject(java.io.ObjectOutputStream);
     private void readObject(java.io.ObjectInputStream);
     java.lang.Object writeReplace();
     java.lang.Object readResolve();
 }

 # 对于带有回调函数的onXXEvent、**On*Listener的，不能被混淆
 -keepclassmembers class * {
     void *(**On*Event);
     void *(**On*Listener);
 }

 # webView处理，项目中没有使用到webView忽略即可  顶点网厅
 -keepclassmembers class com.apexsoft.ddwtl.WebViewActivity {
     *;
 }
 -keepclassmembers class * extends android.webkit.webViewClient {
     public void *(android.webkit.WebView, java.lang.String, android.graphics.Bitmap);
     public boolean *(android.webkit.WebView, java.lang.String);
     public *;
     private *;
 }
 -keepclassmembers class * extends android.webkit.webViewClient {
     public void *(android.webkit.webView, java.lang.String);
     private java.lang.String *(java.lang.String);
     public *;
     private *;
 }

-keep class com.apexsoft.ddwtl.SpjzActivity{ *;}
-keep class org.webrtc.videoengine.**{ *;}
-keep class org.webrtc.voiceengine.**{ *;}
-keep class org.jivesoftware.**{ *;}
-keep class org.jxmpp.stringprep.**{ *;}
-keep class com.apexsoft.vchatengine.**{ *;}
-keep class com.apex.vchat.**{ *;}
-keep class com.lidroid.xutils.**{ *;}
-keep class de.measite.minidns.**{ *;}
#注解不混淆
-keepattributes *Annotation*
-keepattributes *JavascriptInterface*

 # 移除Log类打印各个等级日志的代码，打正式包的时候可以做为禁log使用，这里可以作为禁止log打印的功能使用
 # 记得proguard-android.txt中一定不要加-dontoptimize才起作用
 # 另外的一种实现方案是通过BuildConfig.DEBUG的变量来控制
 #-assumenosideeffects class android.util.Log {
 #    public static int v(...);
 #    public static int i(...);
 #    public static int w(...);
 #    public static int d(...);
 #    public static int e(...);
 #}


#############################################
#
# 项目中特殊处理部分
#
#############################################

#-----------处理反射类---------------



#-----------处理js交互---------------



#-----------处理实体类---------------
# 在开发的时候我们可以将所有的实体类放在一个包内，这样我们写一次混淆就行了。
#-keep  class itsen.com.bduidemo.modle.dmhx.beem.** {
#    public void set*(***);
#    public *** get*();
#    public *** is*();
#}

#itsen.com.bduidemo.modle.dmhx.beem 包下所有的实体类都不被混淆
-keep  class itsen.com.bduidemo.modle.dmhx.beem.** { *; }


#-=-=-=-=-=-=-=-=-=-处理第三方依赖库-=-=-=-=-=-=-=-=-=-=-=-=-=-=-#

# Glide