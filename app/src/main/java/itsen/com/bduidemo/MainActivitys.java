package itsen.com.bduidemo;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
import itsen.com.bduidemo.lib.LibTestActivity;
import itsen.com.bduidemo.lib.tool.ToastTool;
import itsen.com.bduidemo.lib.widget.LDividerItemDecoration;
import itsen.com.bduidemo.lib.zxing.activity.CaptureActivity;
import itsen.com.bduidemo.modle.arr.ArrActivity;
import itsen.com.bduidemo.modle.dmhx.DmhxActivity;
import itsen.com.bduidemo.modle.memorylink.TestActivity;
import itsen.com.bduidemo.modle.net.NetActivity;
import itsen.com.bduidemo.modle.setting.SettingActivity;
import itsen.com.bduidemo.modle.md.MDActivity;
import itsen.com.bduidemo.modle.study.StudyActivity;
import itsen.com.bduidemo.modle.system.SystemMainActivity;
import itsen.com.bduidemo.modle.ui.MainRecycleAdapter;
import itsen.com.bduidemo.modle.ui.SuperUIActivity;
import itsen.com.bduidemo.modle.xrv.XRVActivity;

/**
 * Created by 周德森
 * Created time 2017/6/20 21:54
 * Description:DNui
 * Version: V 1.0
 */

public class MainActivitys extends BaseAppActivity {
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    private RecyclerView recyclerView;
    private MainRecycleAdapter adapter;
    private List<Intent> intentList;
    private List<String> listItenText;

    @Override
    public int getLayoutId() {
        return R.layout.activity_mains;

    }

    @Override
    public void initData() {
        initStatusView();
        intiIntentList();
        recyclerView = (RecyclerView) findViewById(R.id.rv_main);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new LDividerItemDecoration(this,LDividerItemDecoration.VERTICAL_LIST));
        adapter = new MainRecycleAdapter(getListItenten());
        adapter.setOnitemClickListener(new MainRecycleAdapter.OnitemClickListener() {
            @Override
            public void itemClick(View v, int position) {
                startActivity(intentList.get(position).putExtra(KEY, listItenText.get(position)));
            }
        });
        recyclerView.setAdapter(adapter);
        setToolbarTitle("案例列表");
        setToolbarNavGone();
        initNavView();
    }

    private void initStatusView() {

    }

    private List<String> getListItenten() {
        return listItenText;
    }

    private void intiIntentList() {
        intentList = new ArrayList<>();
        listItenText = new ArrayList();
        int index = 0;
        listItenText.add(index, "UI相关");
        intentList.add(index, new Intent(this, SuperUIActivity.class));
        listItenText.add(++index, "网络");
        intentList.add(index, new Intent(this, NetActivity.class));
        listItenText.add(++index, "XRecycleview");
        intentList.add(index, new Intent(this, XRVActivity.class));
        listItenText.add(++index, "5.0新控件");
        intentList.add(index, new Intent(this, MDActivity.class));
        listItenText.add(++index, "android系统学习Demo");
        intentList.add(index, new Intent(this, SystemMainActivity.class));
        listItenText.add(++index, "内存测试");
        intentList.add(index, new Intent(this, TestActivity.class));

        listItenText.add(++index, "android基础");
        intentList.add(index, new Intent(this, StudyActivity.class));

        listItenText.add(++index, "Lib测试");
        intentList.add(index, new Intent(this, LibTestActivity.class));
        listItenText.add(++index, "代码混淆测试");
        intentList.add(index, new Intent(this, DmhxActivity.class));
        listItenText.add(++index, "arr代码混淆测试");
        intentList.add(index, new Intent(this, ArrActivity.class));
        /*listItenText.add(++index, "Sqlite");
        intentList.add(index, new Intent(this, SqliteLitelPalActivity.class));*/
        /*listItenText.add(++index, "事件分发机制（复习案例）");
        intentList.add(index, new Intent(this, EventActivity.class));*/
        /*listItenText.add(++index, "Scoller");
        intentList.add(index, new Intent(this, Scrollerctivity.class));*/


    }

    private void initNavView() {
        //添加头部
        View navheasView = navView.inflateHeaderView(R.layout.nav_head_view);
        //监听初始化
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.nav_me:
                        break;
                    case R.id.nav_about:
                        break;
                    case R.id.nav_friend:
                        break;
                    case R.id.nav_message:
                        startActivityForResult(new Intent(MainActivitys.this, CaptureActivity.class),1);
                        break;
                    case R.id.nav_setting:
                        startActivity(new Intent(MainActivitys.this,SettingActivity.class));
                        break;
                    //......添加更多
                }
                //关闭抽屉
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode ==RESULT_OK){
            if (data!=null){
                ToastTool.showLong(data.getStringExtra("scan_result"));
            }
        }
    }
}
