package itsen.com.bduidemo.modle.ui.canvas;

import android.os.Bundle;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

/**
 * Created by 周德森
 * Created time 2017/6/10 20:46
 * Description:DNui
 * Version: V 1.0
 */

public class CanvasActivity extends BaseAppActivity {
    @BindView(R.id.gbc)
    GabageCan gbc;

    @Override
    public int getLayoutId() {
        return R.layout.activity_canvas;
    }

    @Override
    public void initData() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_start)
    public void onViewClicked() {
        gbc.startAnimator();
    }
}
