package itsen.com.bduidemo.modle.md.al6;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class GarbageClearActivity extends BaseAppActivity {
    private TextView textView;
    private ClearProgressView clearProgressView;
    @Override
    public int getLayoutId() {
        return R.layout.activity_garbage_clear;
    }

    @Override
    public void initData() {
        clearProgressView = (ClearProgressView) findViewById(R.id.clear_view);
        textView = (TextView) findViewById(R.id.tv_show_point);
    }


    public void click(View view) {
       if (view.getId()==R.id.btn_start){
           clearProgressView.runningAinm();
       }else if (view.getId()==R.id.btn_finish){
           textView.setText("98");
           clearProgressView.finishAinm();
       }else if (view.getId()==R.id.btn_reset){
           textView.setText("58");
           clearProgressView.initAnim();
       }
    }
}
