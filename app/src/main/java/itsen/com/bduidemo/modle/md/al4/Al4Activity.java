package itsen.com.bduidemo.modle.md.al4;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import itsen.com.bduidemo.R;

public class Al4Activity extends AppCompatActivity {

    @BindView(R.id.iv_head)
    ImageView ivHead;
    @BindView(R.id.scrollView)
    MyScrollview scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_al4);
        ButterKnife.bind(this);
        scrollView.setmImageview(ivHead);
        //滑动到顶部不显示蓝色阴影
        scrollView.setOverScrollMode(View.OVER_SCROLL_NEVER);
    }
}
