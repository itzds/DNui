package itsen.com.bduidemo.modle.md.al;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.lib.widget.LDividerItemDecoration;
import itsen.com.bduidemo.lib.widget.LMaxHeightRecyclerView;
import itsen.com.bduidemo.modle.ui.MainRecycleAdapter;

/**
 * BottomSheetBehavior  分析源码: behaviors + ViewDragHelper  等完成复杂交互
 */
public class AL1Activity extends AppCompatActivity {

    @BindView(R.id.blackview)
    View blackView;
    @BindView(R.id.elasticView)
    ElasticPathView elasticView;
    @BindView(R.id.car_recyclerview)
    LMaxHeightRecyclerView mRecyclerView;
    @BindView(R.id.car_container)
    LinearLayout carContainer;

    private MainRecycleAdapter mAdapter;
    private List<String> listItenText;
    public BottomSheetBehavior behavior;
    public boolean sheetScrolling = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_al1);
        ButterKnife.bind(this);
        init();
        initBe();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new LDividerItemDecoration(this, LDividerItemDecoration.VERTICAL_LIST));
        mAdapter = new MainRecycleAdapter(listItenText);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void click(View view) {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else {
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
    }

    private void init() {
        listItenText = new ArrayList();
        for (int i = 0; i < 30; i++) {
            listItenText.add("学习" + i);
        }
    }

    private void initBe(){
        behavior = BottomSheetBehavior.from(carContainer);
        if(behavior!=null){
            behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    Log.e("newState",":"+newState);
                    sheetScrolling = false;
                    //折叠或隐藏
                    if (newState == BottomSheetBehavior.STATE_COLLAPSED || newState == BottomSheetBehavior.STATE_HIDDEN) {
                        blackView.setVisibility(View.GONE);
                    }
                    if (newState == BottomSheetBehavior.STATE_SETTLING){
                        elasticView.animPath();
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    sheetScrolling = true;
                    blackView.setVisibility(View.VISIBLE);
                    ViewCompat.setAlpha(blackView, slideOffset);
                }
            });

            //点击背景 隐藏
            blackView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    return true;
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        if (behavior.getState()==BottomSheetBehavior.STATE_EXPANDED){
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }else {
            super.onBackPressed();
        }
    }
}
