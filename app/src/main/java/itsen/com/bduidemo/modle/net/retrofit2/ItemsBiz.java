package itsen.com.bduidemo.modle.net.retrofit2;

import java.util.Map;

import io.reactivex.Observable;
import itsen.com.bduidemo.modle.net.been.Items;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by zhoud on 2018/1/2.
 * 参考资料：http://blog.csdn.net/lmj623565791/article/details/51304204
 */

public interface ItemsBiz {

    //直接拼接
    //http://192.168.0.101:8080/ForAnd/items.action
    @GET("items.action")
    Call<Items>getItems();

    @GET("items.action")
    Observable<Items>getItemsRx();


    //RestFul风格，顺序不能打乱  {*}占位符
    //http://192.168.0.101:8080/ForAnd/items2/19/zhou.action
    @POST("items2/{id}/{name}.action")
    Call<Items>getItemsPost(@Path("id") Integer id, @Path("name")String name);

    @POST("items2/{id}/{name}.action")
    Observable<Items>rxGetItems(@Path("id") Integer id, @Path("name")String name);

    //http://192.168.0.101:8080/ForAnd/items3.action?name=zhou&id=10
    @GET("items3.action")
    Call<Items>getItemsQurey(@Query("name")String name,@Query("id")Integer id);

    //模拟表单提交
    //http://192.168.0.101:8080/ForAnd/items4.action
    @POST("items4.action")
    @FormUrlEncoded
    Call<Items> getItemsForm(@Field("name") String name, @Field("id") Integer id);

    //json交互
    @POST("items5.action")
    Call<Items>getItemsJson(@Body Items items);

    //单文件上传
    @Multipart
    @POST("items6.action")
    //Call<String> upload(@Part MultipartBody.Part file, @Part("description") RequestBody description);
    Call<ResponseBody> upload(@Part MultipartBody.Part file);

    //多文件上传
    @Multipart
    @POST("items7.action")
    Call<Items> uploadMoreFile(@PartMap Map<String, RequestBody> params);

    //文件下载
    //直接用okhttp
}
