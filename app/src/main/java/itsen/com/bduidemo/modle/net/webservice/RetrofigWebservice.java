package itsen.com.bduidemo.modle.net.webservice;

import android.util.Log;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.Strategy;

import java.util.concurrent.TimeUnit;

import itsen.com.bduidemo.MyApplicaiton;
import itsen.com.bduidemo.lib.net.SSLHelper;
import itsen.com.bduidemo.lib.net.UnSafeHostnameVerifier;
import itsen.com.bduidemo.lib.net.rxretrofit.HttpCommonInterceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by zhoud
 * Created time  2018/11/10.
 * Description:
 * Version: V 1.0
 */
public class RetrofigWebservice {
    public final static String BASE_URL = "http://www.webxml.com.cn/WebServices/";
    private static Strategy strategy = new AnnotationStrategy();
    private static Serializer serializer = new Persister(strategy);
    private static HttpLoggingInterceptor loggingInterceptor;
    private static RetrofigWebservice retrofigWebservice;
    private static OkHttpClient okHttpClient;
    private Retrofit mRetrofit;
    private static final int DEFAULT_TIME_OUT = 25;

    static {
        getOkHttpClient();
    }

    private RetrofigWebservice() {
        mRetrofit = new Retrofit.Builder()
                .addConverterFactory(SimpleXmlConverterFactory.create(serializer))
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .build();
    }

    public static OkHttpClient getOkHttpClient() {
        //添加公共参数拦截器
        HttpCommonInterceptor interceptor = new HttpCommonInterceptor.Builder()
                .addHeaderParams("Content-Type", "text/xml;charset=UTF-8")
                .build();

        //日志
        loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                //打印retrofit日志
                Log.i("RetrofitLog", "retrofitBack = " + message);
            }
        });
        //日志等级
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        if (okHttpClient == null) {
            synchronized (OkHttpClient.class) {
                if (okHttpClient == null) {
                    okHttpClient = new OkHttpClient.Builder()
                            //打印拦截器日志
                            .addNetworkInterceptor(loggingInterceptor)
                            //设置连接超时时间
                            .connectTimeout(DEFAULT_TIME_OUT, TimeUnit.SECONDS)
                            //设置读取超时时间
                            .readTimeout(DEFAULT_TIME_OUT, TimeUnit.SECONDS)
                            // 设置写入超时时间
                            .writeTimeout(DEFAULT_TIME_OUT, TimeUnit.SECONDS)
                            .build();
                }
            }
        }
        return okHttpClient;
    }

    public static synchronized RetrofigWebservice getInstance() {
        if (retrofigWebservice == null) {
            synchronized (RetrofigWebservice.class) {
                if (retrofigWebservice == null) {
                    retrofigWebservice = new RetrofigWebservice();
                }
            }
        }
        return retrofigWebservice;
    }

    public <T> T setCreate(Class<T> reqServer) {
        return mRetrofit.create(reqServer);
    }
}
