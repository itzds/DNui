package itsen.com.bduidemo.modle.dmhx.beem;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by zds
 * Created time 2018/3/29 15:31
 * Description: Lombok 的使用
 * Version: V 1.0
 */

public class User {
   private String name;
   private int age;
}
