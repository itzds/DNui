package itsen.com.bduidemo.modle.md.tablayout;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class TabLayoutActivity extends BaseAppActivity {

    @BindView(R.id.tab_ly)
    TabLayout mTablayout;
    @BindView(R.id.vp_contexn)
    ViewPager mViewPager;

    @Override
    public int getLayoutId() {
        return R.layout.activity_tab_layout;
    }

    @Override
    public void initData() {
        AdapterFortab adapter = new AdapterFortab(getSupportFragmentManager(),this);
        mViewPager.setOffscreenPageLimit(4);
        mViewPager.setAdapter(adapter);
        mTablayout.setupWithViewPager(mViewPager);
    }
}
