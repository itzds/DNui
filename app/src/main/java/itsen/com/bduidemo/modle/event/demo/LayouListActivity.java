package itsen.com.bduidemo.modle.event.demo;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
import itsen.com.bduidemo.lib.tool.LogTool;
import itsen.com.bduidemo.lib.tool.ToastTool;
import itsen.com.bduidemo.lib.widget.LDividerItemDecoration;
import itsen.com.bduidemo.modle.ui.MainRecycleAdapter;

public class LayouListActivity extends BaseAppActivity {

    @BindView(R.id.tb_title)
    TextView tbTitle;
    @BindView(R.id.ly_top)
    LinearLayout lyTop;
    @BindView(R.id.ly_context)
    LinearLayout lyContext;
    @BindView(R.id.rv_list)
    MyRecycleView recyclerView;
    @BindView(R.id.tv_ck)
    TextView tvCk;
    private MainRecycleAdapter adapter;
    private List<String> listItenText;
    private BehaviorLayoutList mBehavior;

    @Override
    public int getLayoutId() {
        return R.layout.activity_layou_list;
    }

    @Override
    public void initData() {
        intiIntentList();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new LDividerItemDecoration(this, LDividerItemDecoration.VERTICAL_LIST));
        adapter = new MainRecycleAdapter(listItenText);
        recyclerView.setAdapter(adapter);
        mBehavior = BehaviorLayoutList.from(lyContext);
        getViewSizeforHeight();

    }

    private void intiIntentList() {
        listItenText = new ArrayList();
        for (int i = 0; i < 30; i++) {
            listItenText.add("学习" + i);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getViewSizeforHeight() {
        ViewTreeObserver observer = lyContext.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mBehavior.setTvCkHeight(tvCk.getMeasuredHeight());
                mBehavior.setTopHeight(lyTop.getMeasuredHeight());
                LogTool.e("tvCk:" + tvCk.getMeasuredHeight());
                LogTool.e("lyTop:" + lyTop.getMeasuredHeight());
            }
        });
    }

    @OnClick({R.id.tv_like, R.id.tv_sc, R.id.tv_more})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_like:
                ToastTool.showShort("喜欢");
                break;
            case R.id.tv_sc:
                ToastTool.showShort("收藏");
                break;
            case R.id.tv_more:
                ToastTool.showShort("更多");
                break;
        }
    }
}
