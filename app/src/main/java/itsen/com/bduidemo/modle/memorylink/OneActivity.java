package itsen.com.bduidemo.modle.memorylink;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import itsen.com.bduidemo.R;

public class OneActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        //匿名内部类持有外部类的引用，导致外部无法回收内存
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        },10000000);
    }
}
