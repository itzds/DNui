package itsen.com.bduidemo.modle.md.al3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.SeekBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

//http://blog.csdn.net/z82367825/article/details/51599245
//https://www.cnblogs.com/DarkMaster/p/4618872.html
public class AL3Activity extends BaseAppActivity {

    @BindView(R.id.btn_xf_dout)
    XfPathView btnXfDout;
    @BindView(R.id.seekbar)
    SeekBar seekBar;

    @Override
    public int getLayoutId() {
        return R.layout.activity_al3;
    }

    @Override
    public void initData() {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                btnXfDout.setmPercentY(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        btnXfDout.stopAnim();
    }

    @Override
    protected void onResume() {
        super.onResume();
        btnXfDout.reStartAinm();
    }
}
