package itsen.com.bduidemo.modle.net.retrofit2;

import java.util.List;

import io.reactivex.Observable;
import itsen.com.bduidemo.lib.net.rxretrofit.RetrofitManager;
import itsen.com.bduidemo.modle.net.been.Customer;
import itsen.com.bduidemo.modle.net.been.Items;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by zhouds
 * Created time  2018/10/6.
 * Description:
 * Version: V 1.0
 */
public class ItemLoader extends ObjectLoader{
    private ItemService itemService;

    /**
     * 2、初始化：调用RetrofitManager获得retrofit单例，并creat接口
     */
    public ItemLoader(){
        itemService = RetrofitManager.getInstance().create(ItemService.class);
    }

    /**
     * 1、定义retrofig 接口
     */
    public interface ItemService{

        /**
         * @param count
         * @return
         */
        @GET("itemList.action")
        Observable<List<Items>> getItemsQurey(@Query("count")Integer count);


        /**
         * @param count
         * @return
         */
        @GET("query.action")
        Observable<List<Customer>> getQueryCustomer(@Query("count")Integer count);
    }

    /*----3、供外部调用的方法----*/

    /**
     * 查询
     *
     * @param count
     * @return
     */
    public Observable<List<Items>> getItems( Integer count ){
        return observe(itemService.getItemsQurey(count));
    }

    /**
     * 查询
     *
     * @param count
     * @return
     */
    public Observable<List<Customer>> getCustomers(Integer count){
        return observe(itemService.getQueryCustomer(count));
    }
}
