package itsen.com.bduidemo.modle.study;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
import itsen.com.bduidemo.lib.widget.LDividerItemDecoration;
import itsen.com.bduidemo.modle.event.demo.LayouListActivity;
import itsen.com.bduidemo.modle.ui.MainRecycleAdapter;
import itsen.com.bduidemo.modle.ui.recycleview.RecycleDragActivity;

/**
 * @author  周德森
 * Created time 2017/6/20 21:54
 * Description:基础学习
 * Version: V 1.0
 */
public class StudyActivity extends BaseAppActivity {
    private RecyclerView recyclerView;
    private MainRecycleAdapter adapter;
    private List<Intent> intentList;
    private List<String> listItenText;
    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initData() {
        intiIntentList();
        recyclerView = (RecyclerView) findViewById(R.id.rv_main);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new LDividerItemDecoration(this,LDividerItemDecoration.VERTICAL_LIST));
        adapter = new MainRecycleAdapter(getListItenten());
        adapter.setOnitemClickListener(new MainRecycleAdapter.OnitemClickListener() {
            @Override
            public void itemClick(View v, int position) {
                startActivity(intentList.get(position).putExtra(KEY, listItenText.get(position)));
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private List<String> getListItenten() {
        return listItenText;
    }

    private void intiIntentList() {
        intentList = new ArrayList<>();
        listItenText = new ArrayList();
        int index = 0;
        listItenText.add(index, "recycleView交互、拖拽");
        intentList.add(index, new Intent(this, RecycleDragActivity.class));
        listItenText.add(++index, "综合练习（滑动处理）");
        intentList.add(index, new Intent(this, LayouListActivity.class));
    }

}
