package itsen.com.bduidemo.modle.ui;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
import itsen.com.bduidemo.lib.widget.LDividerItemDecoration;
import itsen.com.bduidemo.modle.event.EventActivity;
import itsen.com.bduidemo.modle.event.demo.LayouListActivity;
import itsen.com.bduidemo.modle.event.scroller.Scrollerctivity;
import itsen.com.bduidemo.modle.event.velocity.VelocityActivity;
import itsen.com.bduidemo.modle.event.viewdraghelper.ViewDraghelperActivity;
import itsen.com.bduidemo.modle.ui.animtion.AnimActivity;
import itsen.com.bduidemo.modle.ui.animtion.AnimActivity2;
import itsen.com.bduidemo.modle.ui.animtion.pxkj.PXKJMainActivity;
import itsen.com.bduidemo.modle.ui.camera.CameraActivity;
import itsen.com.bduidemo.modle.ui.canvas.CanvasActivity;
import itsen.com.bduidemo.modle.ui.datepiker.DatePickerActivity;
import itsen.com.bduidemo.modle.ui.paint.PaintActivity;
import itsen.com.bduidemo.modle.ui.path.PathActivity;
import itsen.com.bduidemo.modle.ui.recycleview.RecycleDragActivity;
import itsen.com.bduidemo.modle.md.transitionanim.TransitionActivity;
import itsen.com.bduidemo.modle.ui.webview.WebActivity;
import itsen.com.bduidemo.modle.ui.xfermode.XfemodeActivity;

/**
 * @author 周德森
 *         Created time 2017/6/20 21:54
 *         Description:UI 相关
 *         Version: V 1.0
 */
public class SuperUIActivity extends BaseAppActivity {
    private RecyclerView recyclerView;
    private MainRecycleAdapter adapter;
    private List<Intent> intentList;
    private List<String> listItenText;

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initData() {
        intiIntentList();
        recyclerView = (RecyclerView) findViewById(R.id.rv_main);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new LDividerItemDecoration(this, LDividerItemDecoration.VERTICAL_LIST));
        adapter = new MainRecycleAdapter(getListItenten());
        adapter.setOnitemClickListener(new MainRecycleAdapter.OnitemClickListener() {
            @Override
            public void itemClick(View v, int position) {
                startActivity(intentList.get(position).putExtra(KEY, listItenText.get(position)));
            }
        });
        recyclerView.setAdapter(adapter);

    }

    private List<String> getListItenten() {
        return listItenText;
    }

    private void intiIntentList() {
        intentList = new ArrayList<>();
        listItenText = new ArrayList();
        int index = 0;
        listItenText.add(index, "recycleView交互、拖拽");
        intentList.add(index, new Intent(this, RecycleDragActivity.class));

        listItenText.add(++index, "点击测试");
        intentList.add(index, new Intent(this, ClikActivity.class));

        listItenText.add(++index, "综合练习（滑动处理）");
        intentList.add(index, new Intent(this, LayouListActivity.class));

        listItenText.add(++index, "xfermode（常用的场景）");
        intentList.add(index, new Intent(this, XfemodeActivity.class));

        listItenText.add(++index, "Paint（基线计算）");
        intentList.add(index, new Intent(this, PaintActivity.class));

        listItenText.add(++index, "path(基础/贝塞尔曲线)");
        intentList.add(index, new Intent(this, PathActivity.class));

        listItenText.add(++index, "Canvas");
        intentList.add(index, new Intent(this, CanvasActivity.class));

        listItenText.add(++index, "3D camera");
        intentList.add(index, new Intent(this, CameraActivity.class));

        listItenText.add(++index, "动画 @属性动画");
        intentList.add(index, new Intent(this, AnimActivity.class));

        listItenText.add(++index, "动画 @画布");
        intentList.add(index, new Intent(this, AnimActivity2.class));

        listItenText.add(++index, "动画 @转场");
        intentList.add(index, new Intent(this, TransitionActivity.class));

        listItenText.add(++index, "动画 @平行空间");
        intentList.add(index, new Intent(this, PXKJMainActivity.class));

        listItenText.add(++index, "时间选择");
        intentList.add(index, new Intent(this, DatePickerActivity.class));

        listItenText.add(++index, "拖拽drag");
        intentList.add(index, new Intent(this, ViewDraghelperActivity.class));
   /*     listItenText.add(++index, "Sqlite");
        intentList.add(index, new Intent(this, SqliteLitelPalActivity.class));*/
        listItenText.add(++index, "事件分发机制（复习案例）");
        intentList.add(index, new Intent(this, EventActivity.class));
        listItenText.add(++index, "Scoller");
        intentList.add(index, new Intent(this, Scrollerctivity.class));
        listItenText.add(++index, "速度");
        intentList.add(index, new Intent(this, VelocityActivity.class));

        listItenText.add(++index, "webview头部加载进度封装");
        intentList.add(index, new Intent(this, WebActivity.class));

    }
}
