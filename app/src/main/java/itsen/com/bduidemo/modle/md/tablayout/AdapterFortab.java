package itsen.com.bduidemo.modle.md.tablayout;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import itsen.com.bduidemo.R;

/**
 * Created by 周德森
 * Created time 2017/10/29 9:51
 * Description:DNui
 * Version: V 1.0
 */

public class AdapterFortab extends FragmentPagerAdapter {

    private String  listTab[]={"新闻","娱乐","运动","科技","生活","军事","体育","社会","汽车"};
    private int colorInt [] = {R.color.lightgreen,R.color.gold,R.color.lightpink,
            R.color.lightblue,R.color.lightgreen,R.color.khaki,
            R.color.honeydew,R.color.lightpink,R.color.lightsalmon};
    private Context context;

    public AdapterFortab(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new FragmentTab();
        Bundle bundle = new Bundle();
        bundle.putInt("bgColorId", colorInt[position]);
        bundle.putInt("pageIndex", position);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return listTab.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return listTab[position];
    }
}
