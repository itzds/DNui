package itsen.com.bduidemo.modle.system.widowmanager;


import android.content.Context;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class WindowManagerActivity extends BaseAppActivity implements View.OnTouchListener{
    private WindowManager.LayoutParams mLayoutParams;
    private WindowManager mWindowManager;
    private View mLyView;
    @Override
    public int getLayoutId() {
        return R.layout.activity_window_manager;
    }

    @Override
    public void initData() {
        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        mLyView = LayoutInflater.from(this).inflate(R.layout.view_layout_window,null);
    }

    public void click(View view){
          /*  mLayoutParams = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT, 0, 0,
                    PixelFormat.TRANSPARENT);
            mLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                    | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED;
            mLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
            mLayoutParams.gravity = Gravity.TOP | Gravity.LEFT;
            mLayoutParams.x = 300;
            mLayoutParams.y = 300;
            mLyView.setOnTouchListener(this);
            mWindowManager.addView(mLyView, mLayoutParams);*/

        mLayoutParams = new WindowManager.LayoutParams(WindowManager.LayoutParams.TYPE_TOAST);
        mLayoutParams.flags =  WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        mLayoutParams.gravity = Gravity.TOP | Gravity.LEFT;
        mLayoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        mLayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        mLayoutParams.x = 300;
        mLayoutParams.y = 300;
        mLyView.setOnTouchListener(this);
        mWindowManager.addView(mLyView, mLayoutParams);

    }

    @Override
    protected void onStop() {
        super.onStop();
        try{
            mWindowManager.removeView(mLyView);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        try{
            mWindowManager.removeView(mLyView);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int rawX = (int) event.getRawX();
        int rawY = (int) event.getRawY();
        int x = (int) event.getX();
        int y = (int) event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                mLayoutParams.x = rawX-mLyView.getWidth();
                mLayoutParams.y = rawY-mLyView.getHeight();
                mWindowManager.updateViewLayout(mLyView, mLayoutParams);
                break;
            }
            case MotionEvent.ACTION_UP: {
                break;
            }
            default:
                break;
        }
        return false;
    }
}
