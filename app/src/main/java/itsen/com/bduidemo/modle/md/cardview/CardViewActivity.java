package itsen.com.bduidemo.modle.md.cardview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
import itsen.com.bduidemo.lib.widget.LRoundAngleImageView;

public class CardViewActivity extends BaseAppActivity {

    @BindView(R.id.rl_layout)
    RelativeLayout rlLayout;
    @BindView(R.id.iv_head1)
    LRoundAngleImageView ivHead1;
    @BindView(R.id.iv_head2)
    LRoundAngleImageView ivHead2;
    @BindView(R.id.iv_head3)
    LRoundAngleImageView ivHead3;
    @BindView(R.id.tv_name1)
    TextView tvName1;
    @BindView(R.id.tv_name2)
    TextView tvName2;
    @BindView(R.id.tv_name3)
    TextView tvName3;
    private final int mDuration = 300;
    private final int mDurationLong = 500;
    @Override
    public int getLayoutId() {
        return R.layout.activity_card_view;
    }

    @Override
    public void initData() {

    }


    private void setAnim(View view,int duration,float delaytime){
        ObjectAnimator animator = ObjectAnimator.ofFloat(view,"translationX",view.getWidth(),-50,0);
        animator.setDuration(duration);
        animator.start();

    }

    private void setAnimValue(final View view, int duration, int delay, float numble){
        view.setVisibility(View.INVISIBLE);
        if (numble==0){
            numble = 1;
        }
        float viewWidth = view.getWidth()*numble;
        PropertyValuesHolder ptraslateY = PropertyValuesHolder.ofKeyframe(View.TRANSLATION_X,
                Keyframe.ofFloat(0,viewWidth),
                Keyframe.ofFloat(.2f,viewWidth*0.8f),
                Keyframe.ofFloat(.4f,viewWidth*0.5f),
                Keyframe.ofFloat(.5f,viewWidth*0.1f),
                Keyframe.ofFloat(.6f,-viewWidth*0.1f),
                Keyframe.ofFloat(.7f,viewWidth*0.0f),
                Keyframe.ofFloat(.8f,viewWidth*0.1f),
                Keyframe.ofFloat(.9f,viewWidth*0.1f),
                Keyframe.ofFloat(1f,0)
        );
        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(view,ptraslateY);
        animator.setDuration(duration).setStartDelay(delay);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                view.setVisibility(View.VISIBLE);
            }
        });
        animator.start();
    }

    public void click(View view){
        setAnim(rlLayout,mDuration,0);
        /*setAnim(ivHead1,mDuration,0);
        setAnim(ivHead2,mDuration,0);
        setAnim(ivHead3,mDuration,0);*/
        setAnimValue(ivHead1,mDurationLong,0,2f);
        setAnimValue(ivHead2,mDurationLong,100,1.5f);
        setAnimValue(ivHead3,mDurationLong,200,0.8f);
    }


}
