package itsen.com.bduidemo.modle.ui.xfermode;

import android.view.View;

import butterknife.BindView;
import butterknife.OnClick;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class XfemodeActivity extends BaseAppActivity {

    @BindView(R.id.view_head)
    HeartMap_DSTIN viewHead;
    @BindView(R.id.view_wave)
    IrregularWaveView_DSTIN viewWave;

    @Override
    public int getLayoutId() {
        return R.layout.activity_xfemode;
    }

    @Override
    public void initData() {

    }

    @OnClick({R.id.btn_xf_sint, R.id.btn_xf_dout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_xf_sint:
               // Toast.makeText(this, "xxx", Toast.LENGTH_LONG).show();
                viewHead.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_xf_dout:
              //  Toast.makeText(this, "222", Toast.LENGTH_LONG).show();
                viewHead.setVisibility(View.GONE);
                viewWave.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        viewWave.recycleR();//必须回收
        viewHead.recycleR();
        super.onDestroy();
    }
}
