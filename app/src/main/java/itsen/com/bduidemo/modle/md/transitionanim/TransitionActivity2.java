package itsen.com.bduidemo.modle.md.transitionanim;

import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class TransitionActivity2 extends BaseAppActivity {

    @Override
    public int getLayoutId() {
        return R.layout.activity_transition2;
    }

    @Override
    public void initData() {
        ImageView imageView = (ImageView) findViewById(R.id.iv_transition2);
        Glide.with(this).load("https://img3.doubanio.com/view/movie_poster_cover/lpst/public/p2496088130.jpg").into(imageView);
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.finishAfterTransition(TransitionActivity2.this);
            }
        });
    }
}
