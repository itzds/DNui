package itsen.com.bduidemo.modle.dmhx.beem;

/**
 * Created by zds
 * Created time 2017/11/28 19:13
 * Description:DNui
 * Version: V 1.0
 */

public class Student {
    String name;
    int age;
    int sex;
    String adr;
    String grede;
    public Student(){
        User user = new User();
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getAdr() {
        return adr;
    }

    public void setAdr(String adr) {
        this.adr = adr;
    }

    public String getGrede() {
        return grede;
    }

    public void setGrede(String grede) {
        this.grede = grede;
    }
}
