package itsen.com.bduidemo.modle.ui.md;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.lib.widget.LNestedScrollView;

public class ToolBarTranlateActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private ImageView imageView;
    private TextView textView;
    private ImageButton imageButton;
    private LNestedScrollView lNestedScrollView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tool_bar_tranlate);
        toolbar = (Toolbar) findViewById(R.id.title_tool_bar);
        imageView = (ImageView) findViewById(R.id.tb_img_bg);
        lNestedScrollView = (LNestedScrollView) findViewById(R.id.lnsv);
        imageView.setAlpha(0.0f);
        initToolbar();
        changeAlhpa();
        initMybarAlpha();
    }

    public void initToolbar(){
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            //去除默认Title显示
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.icon_back);
        }
            // 手动设置才有效果
            toolbar.setTitleTextAppearance(this, R.style.ToolBar_Title);
            toolbar.setSubtitleTextAppearance(this, R.style.Toolbar_SubTitle);
            toolbar.setTitle("战狼2");//标题  没有副标题 标题可能UI要求居中，方案在toolbar中添加一个TextView控件 让他作为标题并设置居中
            toolbar.setSubtitle("吴京主演");//副标题
            toolbar.inflateMenu(R.menu.movie_detail);
            toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.actionbar_more));
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.actionbar_more:// 更多信息
                           // WebViewActivity.loadUrl(MovieDetailActivity.this,mMoreUrl,mMovieName);
                            break;
                    }
                    return false;
                }
            });
    }
    //根据滑动的距离改变透明度
    private void changeAlhpa(){
        lNestedScrollView.setOnScrollChangeListener(new LNestedScrollView.ScrollInterface() {
            @Override
            public void onScrollChange(int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY>=0&&scrollY<=500){
                    imageView.setAlpha(scrollY*1.0f/500);
                    //my
                    textView.setAlpha(scrollY*1.0f/500);
                    imageButton.getBackground().setAlpha((int)((1-(scrollY*1.0f/500))*255));
                }else{
                    imageView.setAlpha(1.0f);
                    //my
                    textView.setAlpha(1.0f);
                    imageButton.getBackground().setAlpha(0);
                }
            }
        });
    }

    //menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.movie_detail, menu);
        return true;
    }

    private void initMybarAlpha(){
        textView = (TextView) findViewById(R.id.r_tv_bg);
        imageButton = (ImageButton) findViewById(R.id.r_btn_back);
        textView.setAlpha(0.0f);
    }
}
