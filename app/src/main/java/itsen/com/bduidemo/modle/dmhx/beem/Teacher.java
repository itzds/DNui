package itsen.com.bduidemo.modle.dmhx.beem;

/**
 * Created by zds
 * Created time 2017/11/28 19:27
 * Description:DNui
 * Version: V 1.0
 */

public class Teacher {
    String name;
    String adr;
    String grede;
    boolean isGreat;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdr() {
        return adr;
    }

    public void setAdr(String adr) {
        this.adr = adr;
    }

    public String getGrede() {
        return grede;
    }

    public void setGrede(String grede) {
        this.grede = grede;
    }

    public boolean isGreat() {
        return isGreat;
    }

    public void setGreat(boolean great) {
        isGreat = great;
    }
}
