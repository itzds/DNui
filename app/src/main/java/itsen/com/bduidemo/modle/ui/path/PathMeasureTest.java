package itsen.com.bduidemo.modle.ui.path;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;

import itsen.com.bduidemo.R;

/**
 * Created by 周德森
 * Created time 2017/7/3 22:01
 * Description:DNui
 * Version: V 1.0
 *
 * 1、PathMeasure用来测量 Path的；
 * 重要方法
 *  setPath 设置Path
 *  getLenth 获取path的长度
 *  boolean getPosTan (float distance, float[] pos, float[] tan)   获取处在distance位置的坐标pos和tan正切值
 1

 */

public class PathMeasureTest extends View{
    private Bitmap mBitmap;
    private Paint mPaint;
    private Path path;
    private PathMeasure pathMeasure;
    private float[] pos;                // 当前点的实际位置
    private float[] tan;                // 当前点的tangent值,用于计算图片所需旋转的角度
    private float currentValue = 0;     // 用于纪录当前的位置比例（0-1）
    private Matrix matrix;

    public PathMeasureTest(Context context) {
        super(context);
        init();
        startAnimotion();
    }

    public PathMeasureTest(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
        startAnimotion();
    }
    private void init(){
        mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.img_fj);
        pathMeasure = new PathMeasure();
        mPaint = new Paint();
        mPaint.setColor(Color.RED);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(10);
        path = new Path();
        matrix = new Matrix();
        path.addCircle(0,0,400, Path.Direction.CW);//添加一个圆
        pos = new float[2];
        tan = new float[2];
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.translate(getWidth()/2,getHeight()/2);
        pathMeasure.setPath(path,false);
        float currenLenth = pathMeasure.getLength()*currentValue;
        pathMeasure.getPosTan(currenLenth,pos,tan );
        matrix.reset();
        float degrees = (float) (Math.atan2(tan[1], tan[0]) * 180.0 / Math.PI); // 计算图片旋转角度
        matrix.postRotate(degrees,mBitmap.getHeight()/2,mBitmap.getHeight()/2);
        matrix.postTranslate(pos[0] - mBitmap.getWidth() / 2, pos[1] - mBitmap.getHeight() / 2);// 将图片绘制中心调整到与当前点重合
        canvas.drawPath(path, mPaint);                                   // 绘制 Path
        canvas.drawBitmap(mBitmap, matrix, mPaint);

    }

    private void startAnimotion(){
        ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f);
        animator.setDuration(3000);
        animator.setInterpolator(new LinearInterpolator());
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                currentValue = (float)animation.getAnimatedValue();
                if (currentValue>=1){
                    currentValue = 0;
                }
                postInvalidate();
            }
        });
        animator.start();

    }
}
