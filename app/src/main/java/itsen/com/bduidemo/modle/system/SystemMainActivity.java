package itsen.com.bduidemo.modle.system;




import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
import itsen.com.bduidemo.lib.widget.LDividerItemDecoration;
import itsen.com.bduidemo.modle.md.tablayout.TabLayoutActivity;
import itsen.com.bduidemo.modle.md.transitionanim.TransitionActivity;
import itsen.com.bduidemo.modle.system.service.ServiceActivity;
import itsen.com.bduidemo.modle.system.widowmanager.WindowManagerActivity;
import itsen.com.bduidemo.modle.ui.MainRecycleAdapter;

public class SystemMainActivity extends BaseAppActivity {
    private RecyclerView recyclerView;
    private MainRecycleAdapter adapter;
    private List<Intent> intentList;
    private List<String> listItenText;
    @Override
    public int getLayoutId() {
        return (R.layout.activity_main);
    }

    @Override
    public void initData() {
        intiIntentList();
        recyclerView = (RecyclerView) findViewById(R.id.rv_main);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new LDividerItemDecoration(this,LDividerItemDecoration.VERTICAL_LIST));
        adapter = new MainRecycleAdapter(getListItenten());
        adapter.setOnitemClickListener(new MainRecycleAdapter.OnitemClickListener() {
            @Override
            public void itemClick(View v, int position) {
                startActivity(intentList.get(position).putExtra(KEY, listItenText.get(position)));
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private List<String> getListItenten() {
        return listItenText;
    }

    private void intiIntentList() {
        intentList = new ArrayList<>();
        listItenText = new ArrayList();
        int index = 0;
        listItenText.add(index, "悬浮");
        intentList.add(index, new Intent(this, WindowManagerActivity.class));
        listItenText.add(++index, "service");
        intentList.add(index, new Intent(this, ServiceActivity.class));
    }

}
