package itsen.com.bduidemo.modle.ui.path;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by 周德森
 * Created time 2017/5/27 11:32
 * Description:DNui
 * Version: V 1.0
 */

public class PathTest extends View {
    public PathTest(Context context) {
        this(context,null);
    }

    public PathTest(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Path path = new Path();
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(20);
        path.moveTo(200,500);
        path.quadTo(500,800,800,500);
        canvas.drawPath(path,paint);
    }
}
