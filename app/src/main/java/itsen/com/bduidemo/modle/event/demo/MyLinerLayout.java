package itsen.com.bduidemo.modle.event.demo;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.Px;
import android.support.v4.view.NestedScrollingParent;
import android.support.v4.view.NestedScrollingParentHelper;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import itsen.com.bduidemo.lib.tool.LogTool;

/**
 * Created by 周德森
 * Created time 2017/10/27 14:02
 * Description:DNui
 * Version: V 1.0
 */

public  class MyLinerLayout extends LinearLayout implements NestedScrollingParent{
    private NestedScrollingParentHelper mNestedScrollingParentHelper;
    private MyRecycleView myRecycleView;
    private TextView mTvCk;
    private int mTvHeight;
    public MyLinerLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mNestedScrollingParentHelper = new NestedScrollingParentHelper(this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mTvCk = (TextView) getChildAt(1);
        mTvHeight = mTvCk.getHeight();
        myRecycleView = (MyRecycleView) getChildAt(3);
    }

    @Override
    public boolean onStartNestedScroll(View child, View target, int nestedScrollAxes) {
        int axes = nestedScrollAxes & ViewCompat.SCROLL_AXIS_VERTICAL;
        LogTool.e("axes: "+axes);
        if (target instanceof MyRecycleView){
            LogTool.e("==消费事件=="+ target.getScrollY());

           // return true;
        }
        return false;
    }

    @Override
    public void onNestedScrollAccepted(View child, View target, int nestedScrollAxes) {
        mNestedScrollingParentHelper.onNestedScrollAccepted(child, target, nestedScrollAxes);
    }

    @Override
    public void onStopNestedScroll(View target) {
        mNestedScrollingParentHelper.onStopNestedScroll(target);
    }

    @Override
    public int getNestedScrollAxes() {
        return mNestedScrollingParentHelper.getNestedScrollAxes()&ViewCompat.SCROLL_AXIS_VERTICAL;
    }

    @Override
    public void onNestedPreScroll(View target, int dx, int dy, int[] consumed) {
       // super.onNestedPreScroll(target, dx, dy, consumed);
          //dy  上移动 是正数，  下移动是负数
        LogTool.e("target:"+target.getScrollY());
        LogTool.e("y: "+dy);
        if (dy>0){
            consumed[1] = 0;
        }else if (!target.canScrollVertically(-1)){
            scrollBy(0,dy);
            consumed[1] = dy;
        }
    }

    @Override
    public void onNestedScroll(View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        LogTool.e("dxConsumed");
        super.onNestedScroll(target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
    }
   /* @Override
    public void scrollTo(@Px int x, @Px int y) {
       LogTool.e("y: "+y);
        super.scrollTo(x, y);
    }*/
}
