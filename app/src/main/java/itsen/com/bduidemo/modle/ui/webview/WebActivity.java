package itsen.com.bduidemo.modle.ui.webview;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import java.util.HashMap;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
import itsen.com.bduidemo.lib.tool.ToastTool;
import itsen.com.bduidemo.lib.util.IPutils;
import itsen.com.bduidemo.lib.view.ProgressView;

/**
 * ProjectName:  物物地图
 * Description:  描述
 * Author:  周德森
 * CreateAt:  2017/3/23 0023 10:48
 * Conpany:  福建第一时间物联网科技投资有限公司
 * Copyright:  2016 www.wwhqj.com  Inc. All rights reserved.
 * 获取图片 https://blog.csdn.net/chencaishengsic/article/details/51016679
 */
public class WebActivity extends BaseAppActivity {


    @BindView(R.id.progress)
    ProgressView progress;
    @BindView(R.id.btn_call_js)
    Button btnCallJs;
    private WebView mWebView;

    private static final int FILE_SELECT_CODE = 0;

    /**
     * 回调图片选择，4.4以下
     */
    private ValueCallback<Uri> mUploadMessage;
    /**
     * 回调图片选择，5.0以上
     */
    private ValueCallback<Uri[]> mUploadCallbackAboveL;

    @Override
    public int getLayoutId() {
        return R.layout.activity_web;
    }

    @Override
    public void initData() {
        mWebView = (WebView) findViewById(R.id.webView);
        initWebView();
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mWebView.canGoBack()) {
                    mWebView.goBack();
                } else {
                    finish();
                }
            }
        });
        // Android版本变量
        final int version = Build.VERSION.SDK_INT;
        btnCallJs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //SDK<4.4
                if (version < 18) {
                    //方式一 、 Android 通过WebView 调js的 callJS（）方法
                    mWebView.loadUrl("javascript:callJS()");
                } else {
                    //方式二、4.4 以上 效率更高
                 /*   mWebView.evaluateJavascript("javascript:callJS()", new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String value) {
                            //此处为 js 返回的结果
                            showDialogs(value,null);
                        }
                    });*/
                    mWebView.loadUrl("javascript:callJS()");
                }
            }
        });
    }

    /**
     *
     */
    private void initWebView() {
        //声明WebSettings子类
        WebSettings webSettings = mWebView.getSettings();

        //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
        webSettings.setJavaScriptEnabled(true);


        //设置自适应屏幕，两者合用
        webSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
        webSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小

        //缩放操作
        webSettings.setSupportZoom(true); //支持缩放，默认为true。是下面那个的前提。
        webSettings.setBuiltInZoomControls(true); //设置内置的缩放控件。若为false，则该WebView不可缩放
        webSettings.setDisplayZoomControls(false); //隐藏原生的缩放控件

        //其他细节操作

        //缓存- 优先使用缓存
        //LOAD_CACHE_ONLY: 不使用网络，只读取本地缓存数据
        //LOAD_DEFAULT: （默认）根据cache-control决定是否从网络上取数据。
        //LOAD_NO_CACHE: 不使用缓存，只从网络获取数据.
        //LOAD_CACHE_ELSE_NETWORK，只要本地有，无论是否过期，或者no-cache，都使用缓存中的数据。
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        //设置可以访问文件
        webSettings.setAllowFileAccess(true);
        //支持通过JS打开新窗口
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        //支持自动加载图片
        webSettings.setLoadsImagesAutomatically(true);
        //设置编码格式
        webSettings.setDefaultTextEncodingName("utf-8");


        //WebViewClient类
        mWebView.setWebViewClient(new WebViewClient() {
            //复写shouldOverrideUrlLoading()方法，使得打开网页时不调用系统浏览器， 而是在本WebView中显示
            //所以要调用本机浏览器的话，不复写这个方法
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                //拦截
                Uri uri = Uri.parse(url);
                // 如果url的协议 = 预先约定的 js 协议
                // 就解析往下解析参数
                if (uri.getScheme().equals("js")) {
                    // 如果 authority  = 预先约定协议里的 webview，即代表都符合约定的协议
                    // 所以拦截url,下面JS开始调用Android需要的方法
                    if (uri.getAuthority().equals("webview")) {
                        //  步骤3：
                        // 执行JS所需要调用的逻辑
                        System.out.println("js调用了Android的方法");
                        // 可以在协议上带有参数并传递到Android上
                        HashMap<String, String> params = new HashMap<>();
                        Set<String> collection = uri.getQueryParameterNames();
                    }
                    return true;
                }

                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                //设定加载开始的操作  显示loading页面
            }

            /*
            作用：在页面加载结束时调用。我们可以关闭loading 条，切换程序动作。
            * */
            @Override
            public void onPageFinished(WebView view, String url) {
                //设定加载结束的操作  关闭加载页
            }

            /*
            作用：加载页面的服务器出现错误时（如404）调用。
            * */
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {

            }

            //            webView默认是不处理https请求的，页面显示空白，需要进行如下设置：
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                // handler.proceed();    //表示等待证书响应
                // handler.cancel();      //表示挂起连接，为默认方式
                // handler.handleMessage(null);    //可做其他处理
            }
        });

        // WebChromeClient类
        mWebView.setWebChromeClient(new WebChromeClient() {
            // 作用：获得网页的加载进度并显示
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                // newProgress  (0-100)
                progress.setProgressValue(newProgress);
                if (newProgress == 100) {
                    progress.setProgressValue(0);
                }
            }

            //作用：获取Web页中的标题
            @Override
            public void onReceivedTitle(WebView view, String title) {
                //标题 title
                setToolbarTitle(title);
            }

            @Override
            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                showDialogs(message, result);
                //return super.onJsAlert(view, url, message, result);
                return true;
            }

            // For Android 3.0+
            public void openFileChooser(ValueCallback<Uri> uploadMsg) {

                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, "File Chooser"), FILE_SELECT_CODE);

            }

            // For Android 3.0+
            public void openFileChooser(ValueCallback uploadMsg, String acceptType) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("*/*");
                startActivityForResult(Intent.createChooser(i, "File Browser"), FILE_SELECT_CODE);
            }

            // For Android 4.1
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, "File Chooser"), FILE_SELECT_CODE);

            }

            // For Android 5.0+
            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                mUploadCallbackAboveL = filePathCallback;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("*/*");
                startActivityForResult(
                        Intent.createChooser(i, "File Browser"),
                        FILE_SELECT_CODE);
                return true;
            }
        });


        // 通过addJavascriptInterface()将Java对象映射到JS对象
        //参数1：Javascript对象名
        //参数2：Java对象名

        //JsCallAndroid类对象映射到js的test对象
        mWebView.addJavascriptInterface(new JsCallAndroid(), "test");
        //mWebView.addJavascriptInterface(this, "mobile");


        //选择加载方式
        //方式1. 加载一个网页：
        //mWebView.loadUrl("https://www.baidu.com/");
        mWebView.loadUrl(IPutils.getIP() + "ForAnd/item/itemlist2.action");
        //mWebView.loadUrl(IPutils.getIP() + "ForAnd/jsandroid.action");

        //方式2：加载apk包中的html页面
        //mWebView.loadUrl("file:///android_asset/test.html");

        //方式3：加载手机本地的html页面
        // mWebView.loadUrl("content://com.android.htmlfileprovider/sdcard/test.html");

    }

    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //它会暂停所有webview的layout，parsing，javascripttimer。降低CPU功耗。
        mWebView.pauseTimers();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        //恢复pauseTimers状态
        mWebView.resumeTimers();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //销毁Webview
        //在关闭了Activity时，如果Webview的音乐或视频，还在播放。就必须销毁Webview
        //但是注意：webview调用destory时,webview仍绑定在Activity上
        //这是由于自定义webview构建时传入了该Activity的context对象
        //因此需要先从父容器中移除webview,然后再销毁webview:
        mWebView.destroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showDialogs(String msg, final JsResult result) {
        final AlertDialog.Builder b = new AlertDialog.Builder(WebActivity.this);
        b.setTitle("Alert");
        b.setMessage(msg);
        b.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (result != null) {
                    result.confirm();
                }
                dialog.dismiss();
            }
        });
        b.setCancelable(false);
        b.create().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case FILE_SELECT_CODE:
                //5.0以上
                if (Build.VERSION.SDK_INT>=21){
                    Uri uri = data.getData();
                    Uri[] ruis = new Uri[]{
                            uri
                    };
                    //回调给js
                    mUploadCallbackAboveL.onReceiveValue(ruis);
                }else {
                    //4.4以下
                }

                break;
            default:
                break;
        }
    }

  /*  @JavascriptInterface
    public void showValue(String value){
        ToastTool.showLong(value);
    }*/
}
