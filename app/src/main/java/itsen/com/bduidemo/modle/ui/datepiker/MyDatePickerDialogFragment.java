package itsen.com.bduidemo.modle.ui.datepiker;

import itsen.com.bduidemo.lib.view.datepicker.date.DatePickerDialogFragment;

/**
 * Created by zhoud on 2018/3/17.
 */

public class MyDatePickerDialogFragment extends DatePickerDialogFragment{
    @Override
    protected void initChild() {
        super.initChild();
        mCancelButton.setTextSize(mCancelButton.getTextSize() + 5);
        mDecideButton.setTextSize(mDecideButton.getTextSize() + 5);
        mDatePicker.setShowCurtain(false);
    }
}
