package itsen.com.bduidemo.modle.sqlite.been;

import org.litepal.annotation.Column;
import org.litepal.crud.DataSupport;

/**
 * Created by 周德森
 * Created time 2017/6/20 22:08
 * Description:DNui
 * Version: V 1.0
 */

public class Student extends DataSupport {
    /**
     * 注解说明：
     * 设置为唯一的unique，不可以为空nullable，忽略此字段映射到列表中ignore
     * 注意: 字段只有声明为 private 才能被映射到数据中
     */
    @Column(unique = true,nullable = false)
    private int id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String number;
    @Column(nullable = false)
    private String mobile;
    @Column(ignore = true)
    private String adr;
    private int age;
    private String grade;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public String getMobile() {
        return mobile;
    }

    public String getAdr() {
        return adr;
    }

    public int getAge() {
        return age;
    }

    public String getGrade() {
        return grade;
    }
}
