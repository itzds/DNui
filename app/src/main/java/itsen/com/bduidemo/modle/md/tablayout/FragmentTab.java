package itsen.com.bduidemo.modle.md.tablayout;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.lib.tool.LogTool;

/**
 * Created by 周德森
 * Created time 2017/10/29 9:14
 * Description:DNui
 * Version: V 1.0
 */

public class FragmentTab extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        int bgColorId = bundle.getInt("bgColorId");
        LogTool.e("int clolor:"+bgColorId);
        int pageIndex = bundle.getInt("pageIndex");
        View view = inflater.inflate(R.layout.fragment_tab, null);
        RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.rl_bg);
        //relativeLayout.setBackgroundColor(bgColorId);
        return view;
    }
}
