package itsen.com.bduidemo.modle.md;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
import itsen.com.bduidemo.lib.widget.LDividerItemDecoration;
import itsen.com.bduidemo.modle.event.beavior.BehaviorActivity;
import itsen.com.bduidemo.modle.event.nestedscrolling.NestedScrollingActivity;
import itsen.com.bduidemo.modle.md.al.AL1Activity;
import itsen.com.bduidemo.modle.md.al2.Al2Activity;
import itsen.com.bduidemo.modle.md.al3.AL3Activity;
import itsen.com.bduidemo.modle.md.al4.Al4Activity;
import itsen.com.bduidemo.modle.md.al5.Al5Activity;
import itsen.com.bduidemo.modle.md.al6.GarbageClearActivity;
import itsen.com.bduidemo.modle.md.appbar.AppBarActivity;
import itsen.com.bduidemo.modle.md.appbar.AppBarActivity2;
import itsen.com.bduidemo.modle.md.cardview.CardViewActivity;
import itsen.com.bduidemo.modle.md.floatingactionbutton.FloatingActionButtonActivity;
import itsen.com.bduidemo.modle.md.glide.GlideGsmhActivity;
import itsen.com.bduidemo.modle.md.palette.PaletteActivity;
import itsen.com.bduidemo.modle.md.recycleview.HeadFootRecycleviewActivity;
import itsen.com.bduidemo.modle.md.tablayout.TabLayoutActivity;
import itsen.com.bduidemo.modle.md.transitionanim.TransitionActivity;
import itsen.com.bduidemo.modle.ui.MainRecycleAdapter;
import itsen.com.bduidemo.modle.ui.md.StatusBarActivity;
import itsen.com.bduidemo.modle.ui.md.StatusBarActivity2;
import itsen.com.bduidemo.modle.ui.md.ToolBarTranlateActivity;

public class MDActivity extends BaseAppActivity {
    private RecyclerView recyclerView;
    private MainRecycleAdapter adapter;
    private List<Intent> intentList;
    private List<String> listItenText;
    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initData() {
        intiIntentList();
        recyclerView = (RecyclerView) findViewById(R.id.rv_main);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new LDividerItemDecoration(this,LDividerItemDecoration.VERTICAL_LIST));
        adapter = new MainRecycleAdapter(getListItenten());
        adapter.setOnitemClickListener(new MainRecycleAdapter.OnitemClickListener() {
            @Override
            public void itemClick(View v, int position) {
                startActivity(intentList.get(position).putExtra(KEY, listItenText.get(position)));
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private List<String> getListItenten() {
        return listItenText;
    }

    private void intiIntentList() {
        intentList = new ArrayList<>();
        listItenText = new ArrayList();
        int index = 0;
        listItenText.add(index, "动画@转场");
        intentList.add(index, new Intent(this, TransitionActivity.class));
        listItenText.add(++index, "tablalyout");
        intentList.add(index, new Intent(this, TabLayoutActivity.class));
        listItenText.add(++index, "案例1(BottomSheetBehavior实现组合控件)");
        intentList.add(index, new Intent(this, AL1Activity.class));
        listItenText.add(++index, "案例2（广告的另一种实现）");
        intentList.add(index, new Intent(this, Al2Activity.class));
        listItenText.add(++index, "案例3（容器中水波纹）");
        intentList.add(index, new Intent(this, AL3Activity.class));
        listItenText.add(++index, "案例4（视差放大）");
        intentList.add(index, new Intent(this, Al4Activity.class));
        listItenText.add(++index, "案例5（翻頁）");
        intentList.add(index, new Intent(this, Al5Activity.class));
        listItenText.add(++index, "案例6（综合绘制View）");
        intentList.add(index, new Intent(this, GarbageClearActivity.class));
        listItenText.add(++index, "AppbarLayout");
        intentList.add(index, new Intent(this, AppBarActivity.class));
        listItenText.add(++index, "AppbarLayout2");
        intentList.add(index, new Intent(this, AppBarActivity2.class));

        listItenText.add(++index, "NestedScrolling机制");
        intentList.add(index, new Intent(this, NestedScrollingActivity.class));
        listItenText.add(++index, "toolnar");
        intentList.add(index, new Intent(this, ToolBarTranlateActivity.class));
        listItenText.add(++index, "statusBar");
        intentList.add(index, new Intent(this, StatusBarActivity.class));
        listItenText.add(++index, "statusBar2");
        intentList.add(index, new Intent(this, StatusBarActivity2.class));

        listItenText.add(++index, "behavior");
        intentList.add(index, new Intent(this, BehaviorActivity.class));

        listItenText.add(++index, "Palette");
        intentList.add(index, new Intent(this, PaletteActivity.class));
        listItenText.add(++index, "Recycleiew");
        intentList.add(index, new Intent(this, HeadFootRecycleviewActivity.class));
        listItenText.add(++index, "CardView");
        intentList.add(index, new Intent(this, CardViewActivity.class));
        listItenText.add(++index, "floatActionButton");
        intentList.add(index, new Intent(this, FloatingActionButtonActivity.class));
        listItenText.add(++index, "高斯模糊");
        intentList.add(index, new Intent(this, GlideGsmhActivity.class));
    }

}
