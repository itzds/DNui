package itsen.com.bduidemo.modle.ui.listview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import butterknife.BindView;
import butterknife.ButterKnife;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
/**
 * 事实证明：ListView可以添加多个headView或者footView
 */

public class ListViewActivity extends BaseAppActivity {

    @BindView(R.id.listview)
    ListView listview;

    @Override
    public int getLayoutId() {
        return R.layout.activity_list_view;
    }

    @Override
    public void initData() {
        View viewHead = LayoutInflater.from(this).inflate(R.layout.listitem,null);
        View viewHead2 = LayoutInflater.from(this).inflate(R.layout.listitem,null);
        View viewHead3 = LayoutInflater.from(this).inflate(R.layout.listitem,null);
        String [] arrarStr={"刘备","关羽","张飞","马超","黄忠","刘备","关羽","张飞","马超","黄忠","刘备","关羽","张飞","马超","黄忠"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrarStr);
        listview.addHeaderView(viewHead);
        listview.addHeaderView(viewHead2);
        listview.addHeaderView(viewHead3);
        listview.setAdapter(adapter);
    }

}
