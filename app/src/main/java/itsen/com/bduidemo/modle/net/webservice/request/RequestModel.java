package itsen.com.bduidemo.modle.net.webservice.request;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

/**
 * Created by zhoud
 * Created time  2018/11/10.
 * Description: 请求的数据
 * Version: V 1.0
 *
 * <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:enc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
 * <soapenv:Body>
 * <getWeatherbyCityName xmlns="http://WebXml.com.cn/">
 * <theCityName>上海</theCityName>
 * </getWeatherbyCityName>
 * </soapenv:Body>
 * </soapenv:Envelope>
 */
public class RequestModel {
    @Attribute(name = "xmlns")
    public String cityNameAttribute;

    @Element(name = "theCityName", required = false)
    public String theCityName;

    //增加其他请求字段
}
