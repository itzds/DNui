package itsen.com.bduidemo.modle.event.nestedscrolling;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class NestedScrollingActivity extends BaseAppActivity {

    @Override
    public int getLayoutId() {
        return R.layout.activity_nested_scrolling;
    }

    @Override
    public void initData() {

    }
}
