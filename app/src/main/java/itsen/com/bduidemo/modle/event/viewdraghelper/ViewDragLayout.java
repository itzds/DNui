package itsen.com.bduidemo.modle.event.viewdraghelper;

import android.content.Context;
import android.graphics.Point;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import itsen.com.bduidemo.lib.tool.LogTool;

/**
 * Created by 周德森
 * Created time 2017/10/20 13:54
 * Description:DNui
 * Version: V 1.0
 */

public class ViewDragLayout extends LinearLayout {
    private ViewDragHelper mDragheiper;
    private View mDragView1;
    private View mDragView2;
    /**
     * 组边移动捕获
     */
    private View mDragView3;
    /**
     * button
     */
    private View mDragView4;
    private Point point = new Point();
    private int mWidth;
    private int mHeight;
    private View btn;

    public ViewDragLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initDraghelper();
    }

    private void initDraghelper(){
        //初始化ViewDragHelper
        /**
         * create 创建
         * 三个参数：
         * 第一个：父布局-- 监听
         * 第二个：拖拽敏感系数--  1.0f是正常值，越大越敏感
         * 第三个：回调对象-- 相关信息和接收事件用
         */

        mDragheiper = ViewDragHelper.create(this, 0.1f, new ViewDragHelper.Callback() {
            //TODO 捕获view，如果想让其可以拖拽返回true
            @Override
            public boolean tryCaptureView(View child, int pointerId) {
                return child == mDragView1|| child == mDragView2||child == mDragView4;
            }

            //TODO 支持水平拖拽    移动范围控制
            @Override
            public int clampViewPositionHorizontal(View child, int left, int dx) {
                if (left<=0){
                    return 0;
                } else if ((child.getMeasuredWidth()+left)>=getMeasuredWidth()){
                    return getMeasuredWidth()-child.getMeasuredWidth();
                }
                return left;
            }

            //TODO 支持垂直拖拽
            @Override
            public int clampViewPositionVertical(View child, int top, int dy) {
                if (child != mDragView1){
                    return top;
                }
                return  super.clampViewPositionVertical(child,top,dy);
            }

            //TODO 手指释放时回调操作
            @Override
            public void onViewReleased(View releasedChild, float xvel, float yvel) {
                if (releasedChild == mDragView2){
                    //设置别捕获的view 的位置
                    mDragheiper.settleCapturedViewAt(point.x,point.y);
                    invalidate();
                }
            }

            @Override
            public void onViewCaptured(View capturedChild, int activePointerId) {
                LogTool.e("view:"+capturedChild.toString());
            }

            //TODO 手指从边界触摸是回调
            @Override
            public void onEdgeTouched(int edgeFlags, int pointerId) {
                super.onEdgeTouched(edgeFlags, pointerId);
                LogTool.e("edgeFlags","t:"+edgeFlags);
            }
            //
            @Override
            public boolean onEdgeLock(int edgeFlags) {
                LogTool.e("edgeFlags",""+edgeFlags);
                return true;
            }
            //TODO 在边界开始拖动时回调  即将手指在父布局边界拖动 mDragView3 将响应
            @Override
            public void onEdgeDragStarted(int edgeFlags, int pointerId) {
                mDragheiper.captureChildView(mDragView3,pointerId);
            }

            //Button 或view的点击事件
            @Override
            public int getViewHorizontalDragRange(View child) {
                return getMeasuredWidth()-child.getMeasuredWidth()/2;
            }

            @Override
            public int getViewVerticalDragRange(View child) {
                return getMeasuredHeight()-child.getMeasuredHeight()/2;
            }
        });
        //设定父布局边缘跟踪 方向
        mDragheiper.setEdgeTrackingEnabled(ViewDragHelper.EDGE_LEFT);

    }

    /**
     * 事件拦截交给mDragheiper决定
     * @param ev
     * @return
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return mDragheiper.shouldInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //处理事件
        mDragheiper.processTouchEvent(event);
        return true;
    }

    //内部的机制也是soller，要平滑过渡就必须实现该方法
    @Override
    public void computeScroll() {
        if (mDragheiper.continueSettling(true)){
            invalidate();
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        point.x = mDragView2.getLeft();
        point.y = mDragView2.getTop();
    }

    /**
     * 布局加载后 获取布局中的三个子view
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mDragView1 = getChildAt(1);
        mDragView2 = getChildAt(2);
        mDragView3 = getChildAt(3);
        mDragView4 = getChildAt(4);
    }
}
