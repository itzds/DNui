package itsen.com.bduidemo.modle.ui.canvas;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by 周德森
 * Created time 2017/5/26 10:37
 * Description: Canvas saveLayer
 * Version: V 1.0
 */

public class CanvasViewLayer extends View {
    private Paint mPaint;
    public CanvasViewLayer(Context context) {
        this(context,null);
    }

    public CanvasViewLayer(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        RectF rectF = new RectF(0,0,500,500);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(10);
        mPaint.setColor(Color.RED);

        canvas.drawRect(rectF,mPaint);//画一个

        canvas.translate(20,20);
        /*
        * 1、新建layer
        * */
        int layer = canvas.saveLayer(0,0,canvas.getWidth(),canvas.getHeight(),mPaint,Canvas.ALL_SAVE_FLAG);
        /*
        * 2、再新建得layer上画操作
        * */
        canvas.drawColor(Color.BLUE);
        mPaint.setColor(Color.GREEN);
        canvas.drawRect(rectF,mPaint);//画一个
        /*
        * 3、出栈 将layer上得图像画到canvas上   canvas.restore();/canvas.restoreToCount(layer);
        * */
        canvas.restoreToCount(layer);
    }
}
