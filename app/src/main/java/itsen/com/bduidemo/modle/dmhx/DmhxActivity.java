package itsen.com.bduidemo.modle.dmhx;

import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;

import butterknife.ButterKnife;
import butterknife.OnClick;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
import itsen.com.bduidemo.lib.tool.ToastTool;
import itsen.com.bduidemo.modle.dmhx.beem.Student;
import itsen.com.bduidemo.modle.dmhx.interf.ClickLinsener;

public class DmhxActivity extends BaseAppActivity {

    @Override
    public int getLayoutId() {
        return R.layout.activity_dmhx;
    }

    @Override
    public void initData() {

    }



    @OnClick({R.id.btn_been_gson, R.id.btn_other})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_been_gson:
                Gson gson = new Gson();
                Student student = new Student();
                student.setAdr("上海市");
                student.setGrede("高一（2）班");
                student.setAge(12);
                student.setName("张小斐");
                ToastTool.showLong(gson.toJson(student));
                break;
            case R.id.btn_other:
                ClickLinsener linsener = new ClickLinsener();
                linsener.click(new ClickLinsener.MyClikLinsener() {
                    @Override
                    public void onClick(String s) {
                        ToastTool.showLong("内部类被执行了");
                    }
                });

                break;
        }
    }

    interface Mylinser{
        void click(String s);
    }
}
