package itsen.com.bduidemo.modle.md.transitionanim;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class TransitionActivity extends BaseAppActivity {

    private ImageView imageView;

    @Override
    public int getLayoutId() {
        return R.layout.activity_transition;
    }

    @Override
    public void initData() {
        imageView = (ImageView) findViewById(R.id.iv_transition);
        Glide.with(this).load("https://img3.doubanio.com/view/movie_poster_cover/lpst/public/p2496088130.jpg").into(imageView);
    }



    public void click(View view){
        ActivityOptionsCompat comapt= ActivityOptionsCompat.
                makeSceneTransitionAnimation(this, imageView, "tran123");
        //跳转
        ActivityCompat.startActivity(this,new Intent(this,TransitionActivity2.class),comapt.toBundle());
    }
}
