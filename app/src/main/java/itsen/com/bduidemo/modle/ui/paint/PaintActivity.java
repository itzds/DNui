package itsen.com.bduidemo.modle.ui.paint;

import java.util.Random;

import butterknife.BindView;
import butterknife.OnClick;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
import itsen.com.bduidemo.lib.tool.LogTool;

public class PaintActivity extends BaseAppActivity {

    @BindView(R.id.rpView)
    RoundPercentView rpView;
    Random random = new Random();

    @Override
    public int getLayoutId() {
        return R.layout.activity_paint;
    }

    @Override
    public void initData() {
        rpView.setProgress(60);
    }

    @OnClick(R.id.rpView)
    public void onViewClicked() {
        int num = random.nextInt(100);
        LogTool.e("num", num + "");
        rpView.setProgress(num);
    }
}
