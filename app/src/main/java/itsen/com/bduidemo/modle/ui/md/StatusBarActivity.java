package itsen.com.bduidemo.modle.ui.md;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import butterknife.BindView;
import butterknife.ButterKnife;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.lib.tool.LogTool;
import itsen.com.bduidemo.lib.util.StatusBarUtil;
import itsen.com.bduidemo.lib.util.StatusBarUtils;
import itsen.com.bduidemo.lib.widget.LNestedScrollView;
import jp.wasabeef.glide.transformations.BlurTransformation;

public class StatusBarActivity extends AppCompatActivity {

    @BindView(R.id.img_r_bg)
    ImageView imgRBg;
    @BindView(R.id.img_music)
    ImageView imgMusic;
    @BindView(R.id.tv_ms_jj)
    TextView tvMsJj;
    @BindView(R.id.tb_img_bg)
    ImageView tbImgBg;
    @BindView(R.id.lnesv)
    LNestedScrollView lnesv;
    @BindView(R.id.title_tool_bar)
    Toolbar toolbar;
    private int imageBgHeight;//高斯图背景的高度
    private int slidingDistance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_bar);
        ButterKnife.bind(this);
        initSlideTheme();
        initToolbar();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            //去除默认Title显示
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.icon_back);
        }
        // 手动设置才有效果
        toolbar.setTitleTextAppearance(this, R.style.ToolBar_Title);
        toolbar.setSubtitleTextAppearance(this, R.style.Toolbar_SubTitle);
        toolbar.setTitle("类似网易云UI");//标题  没有副标题 标题可能UI要求居中，方案在toolbar中添加一个TextView控件 让他作为标题并设置居中
        toolbar.setSubtitle("我爱唱歌");//副标题
        toolbar.inflateMenu(R.menu.movie_detail);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.actionbar_more));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.actionbar_more:// 更多信息
                        break;
                }
                return false;
            }
        });
    }

    private void initSlideTheme(){
        setimgBg();// 背景设置
        int toolBarHeight = toolbar.getLayoutParams().height;//toolbar的高度
        int statusBarHeit = StatusBarUtil.getStatusBarHeight(this);//statusBar的高度
        int headerBgheight = toolBarHeight + statusBarHeit;//toolbar+statusbar
        LogTool.e("head",""+headerBgheight);
        // 使背景图向上移动到图片的最低端，保留（toolbar+statusbar）的高度
        ViewGroup.LayoutParams params = tbImgBg.getLayoutParams();
        ViewGroup.MarginLayoutParams toolBarImgbgHeadBgParams = (ViewGroup.MarginLayoutParams) tbImgBg.getLayoutParams();
        int marginTop = params.height - headerBgheight;
        slidingDistance = marginTop;
        LogTool.e("head","top:"+marginTop);
        //toolbal的背景向上移动
        toolBarImgbgHeadBgParams.setMargins(0,-marginTop,0,0);
        tbImgBg.setImageAlpha(0);
        StatusBarUtils.setTranslucentImageHeader(this,0,toolbar);
        ViewGroup.LayoutParams imgItemBgparams = imgRBg.getLayoutParams();
        // 获得高斯图背景的高度
        imageBgHeight = imgItemBgparams.height;
        LogTool.e("head","img:"+imageBgHeight);
        initSlideParams();
        initNesv();
    }

    private void initSlideParams(){
        //slidingDistance = imageBgHeight - StatusBarUtil.getStatusBarHeight(this)-toolbar.getHeight();
        //slidingDistance = imageBgHeight - 213;
    }

    private void scrollChangeHeader(int scrolledY) {
        if (scrolledY < 0) {
            scrolledY = 0;
        }
        float alpha = Math.abs(scrolledY) * 1.0f / (slidingDistance);

        Drawable drawable = tbImgBg.getDrawable();
        if (scrolledY <= slidingDistance) {
            // title部分的渐变
            drawable.mutate().setAlpha((int) (alpha * 255));
            tbImgBg.setImageDrawable(drawable);
        } else {
            drawable.mutate().setAlpha(255);
            tbImgBg.setImageDrawable(drawable);
        }
    }
    //设置高斯模糊背景
    private void setimgBg() {
        String imagUrl = "https://img3.doubanio.com/view/photo/thumb/public/p2485983612.jpg";
        //String imagUrl = "";
     /*   Glide.with(this).load(imagUrl)
                .error(R.drawable.stackblur_default)
                .bitmapTransform(new BlurTransformation(this, 23, 4)).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                toolbar.setBackgroundColor(Color.TRANSPARENT);
                tbImgBg.setImageAlpha(0);
                return false;
            }
        }).into(tbImgBg);*/

        /*Glide.with(this).load(imagUrl).error(R.drawable.stackblur_default).bitmapTransform(new BlurTransformation(this, 23, 4)).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                return false;
            }
        }).into(imgRBg);*/
        Glide.with(this).load(R.drawable.ic_music_hread).bitmapTransform(new BlurTransformation(this,23,4)).into(tbImgBg);
        Glide.with(this).load(R.drawable.ic_music_hread).bitmapTransform(new BlurTransformation(this,23,4)).into(imgRBg);
    }

    private void initNesv() {
        lnesv.setOnScrollChangeListener(new LNestedScrollView.ScrollInterface() {
            @Override
            public void onScrollChange(int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                LogTool.e("scrollY:"+scrollY);
                scrollChangeHeader(scrollY);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.movie_detail, menu);
        return true;
    }
}
