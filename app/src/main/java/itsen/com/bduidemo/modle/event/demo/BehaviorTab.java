package itsen.com.bduidemo.modle.event.demo;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.lib.tool.LogTool;

/**
 * Created by 周德森
 * Created time 2017/10/28 14:48
 * Description:DNui
 * Version: V 1.0
 */

public class BehaviorTab extends CoordinatorLayout.Behavior<LinearLayout> {
    public BehaviorTab(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, LinearLayout child, View dependency) {
        return dependency.getId() == R.id.ly_context;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, LinearLayout child, View dependency) {

        LogTool.e("dependency","y:"+dependency.getY()+"height:"+child.getHeight());
        if (dependency.getY()<=child.getHeight()*2){
            float degree = 90*(dependency.getY()/(child.getHeight()*2));
            child.setPivotY(0);
            LogTool.e("degree:"+degree);
            child.setRotationX(-degree);
        }else {
            child.setRotationX(-90f);
        }
        return true;
    }
}
