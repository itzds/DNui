package itsen.com.bduidemo.modle.system.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import itsen.com.bduidemo.lib.tool.LogTool;

/**
 * Created by zhoud on 2018/2/4.
 */

public class ServiceStype extends Service {
    private final String TAG = "TestService1";
    //必须要实现的方法
    @Override
    public IBinder onBind(Intent intent) {
        LogTool.e(TAG, "onBind方法被调用!");
        return null;
    }

    //Service被创建时调用
    @Override
    public void onCreate() {
        LogTool.e(TAG, "onCreate方法被调用!");
        super.onCreate();
    }

    //Service被启动时调用
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogTool.e(TAG, "onStartCommand方法被调用!");
        return super.onStartCommand(intent, flags, startId);
    }

    //Service被关闭之前回调
    @Override
    public void onDestroy() {
        LogTool.e(TAG, "onDestory方法被调用!");
        super.onDestroy();
    }

}
