package itsen.com.bduidemo.modle.ui.webview;

import android.util.Log;
import android.webkit.JavascriptInterface;

import itsen.com.bduidemo.lib.tool.ToastTool;

/**
 * Created by zhoud
 * Created time  2018/9/20.
 * Description:
 * Version: V 1.0
 */
public class JsCallAndroid extends Object {

    @JavascriptInterface
    public void hello(String msg) {
        Log.e("js","JS调用了Android的hello方法");
    }

    @JavascriptInterface
    public void showValue(String value){
        ToastTool.showLong(value);
    }
}
