package itsen.com.bduidemo.modle.md.elm;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class ElmActivity extends BaseAppActivity {

    @Override
    public int getLayoutId() {
        return R.layout.activity_elm;
    }

    @Override
    public void initData() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elm);
    }
}
