package itsen.com.bduidemo.modle.md;

import java.util.ArrayList;
import java.util.List;

import itsen.com.bduidemo.modle.md.palette.Goods;

/**
 * Created by 周德森
 * Created time 2017/10/30 19:31
 * Description:DNui
 * Version: V 1.0
 */

public class DataUtils {
    public static String imgUrl[] = {
            "https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2483150767.jpg",
            "https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2502013123.jpg",
            "https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2493581990.jpg",
            "https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2500155563.jpg",
            "https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2502222821.jpg",
            "https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2496995622.jpg",
            "https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2502553277.jpg",
            "https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2502663075.jpg",
            "https://img3.doubanio.com/view/photo/m/public/p2500126054.jpg",
            "https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2501652013.jpg"
    };

    public static String names[] = {
            "高丽得劲", "美丽的世界", "ADDEGD", "HER GOOD FOE", "ANDR GGS", "东方", "我们", "到底", "到底", "三色饭"
    };

    public static List<Goods>  getListGoods(){
        List<Goods>  list = new ArrayList<>();
        for (int i = 0; i < imgUrl.length; i++) {
            Goods goods = new Goods();
            goods.logoUrl = imgUrl[i];
            goods.name = names[i];
            goods.price = "¥" + (67 + i);
            goods.countAndCompoment = "月销售120分  好评率96%";
            list.add(goods);
        }
        return list;
    }
}
