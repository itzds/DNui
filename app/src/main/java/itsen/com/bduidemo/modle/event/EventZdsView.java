package itsen.com.bduidemo.modle.event;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by 周德森
 * Created time 2017/7/9 21:11
 * Description:DNui
 * Version: V 1.0
 *
 * 知识点：
 * 1、view 重写 onTouchEvent方法  返回true和false它的click事情都不执行
 *       只有return super.onTouchEvent(event) click才执行
 * 2、view 设置 onTouchEvent事件 返回true 它的click事情不执行，false执行
 *
 */

public class EventZdsView extends View {
    private String TAG = "touch";
    public EventZdsView(Context context) {
        this(context,null);
    }

    public EventZdsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

    }



  /*  @Override
    public boolean onTouchEvent(MotionEvent event) {
        LogTool.e(TAG,"EventZdsView  onTouchEvent:"+event.getAction());
        return super.onTouchEvent(event);
       // return false;
    }*/
}
