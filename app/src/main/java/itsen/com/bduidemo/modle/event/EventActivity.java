package itsen.com.bduidemo.modle.event;

import android.view.MotionEvent;
import android.view.View;

import butterknife.BindView;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
import itsen.com.bduidemo.lib.tool.LogTool;

public class EventActivity extends BaseAppActivity {
    @BindView(R.id.v_event)
    EventZdsView vEvent;
    @BindView(R.id.v_event_g)
    EventZdsViewGroup viewGroup;
    private String TAG = "touch";

    @Override
    public int getLayoutId() {
        return R.layout.activity_event;
    }

    @Override
    public void initData() {
        vEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              // LogTool.e(TAG, "btn onclik");
            }
        });
        vEvent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                LogTool.e(TAG, "btn onTouch:"+event.getAction());
                //vEvent.getParent().requestDisallowInterceptTouchEvent(true); //不让父空间拦截事件 待测
                return false;
            }
        });
        viewGroup.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
               // LogTool.e(TAG, "viewGroup onTouch:"+event.getAction());
                return false;
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        LogTool.e(TAG, "Activity  dispatchTouchEvent:" + ev.getAction());
        return super.dispatchTouchEvent(ev);
       // viewGroup.dispatchTouchEvent(ev);
       // return true;
    }



    @Override
    public boolean onTouchEvent(MotionEvent event) {
        LogTool.e(TAG, "Activity  onTouchEvent:" + event.getAction());
        return super.onTouchEvent(event);
        //return true;
    }
}
