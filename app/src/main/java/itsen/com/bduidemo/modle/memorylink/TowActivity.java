package itsen.com.bduidemo.modle.memorylink;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import itsen.com.bduidemo.R;

public class TowActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        //让 TestActivity 静态持有该Activity引用 造成 内存无法回收
        TestActivity.activity = this;
    }
}
