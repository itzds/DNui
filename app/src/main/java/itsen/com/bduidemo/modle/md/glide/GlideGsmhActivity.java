package itsen.com.bduidemo.modle.md.glide;

import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
import itsen.com.bduidemo.lib.percentlayout.PercentRelativeLayout;
import itsen.com.bduidemo.lib.widget.LRoundAngleImageView;
import jp.wasabeef.glide.transformations.BlurTransformation;

public class GlideGsmhActivity extends BaseAppActivity {

    @BindView(R.id.riv)
    LRoundAngleImageView riv;
    @BindView(R.id.iv_bg)
    ImageView imageView;
    @BindView(R.id.rl_bg)
    PercentRelativeLayout rlBg;

    @Override
    public int getLayoutId() {
        return R.layout.activity_glide_gsmh;
    }

    @Override
    public void initData() {

       // Glide.with(context).load(R.drawable.defalut_photo).bitmapTransform(new BlurTransformation(context, radius)).into(mImageView);
        Glide.with(this).load(R.drawable.ic_default).bitmapTransform(new BlurTransformation(this,22)).into(imageView);
    }

}
