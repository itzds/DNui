package itsen.com.bduidemo.modle.system.service;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class ServiceActivity extends BaseAppActivity {

    @BindView(R.id.tv_show)
    TextView tvShow;
    private Intent intent1;
    private Intent intent2;
    private Intent intent3;
    /**
     * 保持所启动的Service的IBinder对象
     */
    private ServiceBtype.MyBinder myBinder;
    /**
     * 同时定义一个ServiceConnection对象
     */
    private ServiceConnection conn = new ServiceConnection() {

        //Activity与Service断开连接时回调该方法
        @Override
        public void onServiceDisconnected(ComponentName name) {
            System.out.println("------Service DisConnected-------");
        }

        //Activity与Service连接成功时回调该方法
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            System.out.println("------Service Connected-------");
            myBinder = (ServiceBtype.MyBinder) service;
            myBinder.getService().setCallBack(new ServiceBtype.CallBack() {
                @Override
                public void call(final String msg) {
                    ServiceActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvShow.setText(msg);
                        }
                    });
                }
            });
        }
    };


    @Override
    public int getLayoutId() {
        return R.layout.activity_service;
    }

    @Override
    public void initData() {
        intent1 = new Intent();
        intent1.setAction("com.itsen.example.service.SERVICE1");
        intent1.setPackage(getPackageName());//5.0之后必须设置  现实调用
        intent2 = new Intent();
        intent2.setAction("com.itsen.example.service.SERVICE2");
        intent2.setPackage(getPackageName());
        intent3 = new Intent();
        intent3.setAction("com.itsen.example.service.SERVICE3");
        intent3.setPackage(getPackageName());

    }


    @OnClick({R.id.btn_start_st, R.id.btn_start_st_stop, R.id.btn_start_bt, R.id.btn_start_bt_b, R.id.btn_start_bt_u, R.id.btn_start_bt_stop})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_start_st:
                //启动服务 start方式
                startService(intent1);
                break;
            case R.id.btn_start_st_stop:
                //停止服务
                stopService(intent1);
                break;
            case R.id.btn_start_bt:
                //若是start被调用 unbindService service不一定回收
                startService(intent2);
                break;
            case R.id.btn_start_bt_b:
                //绑定service
                bindService(intent2, conn, Service.BIND_AUTO_CREATE);
                break;
            case R.id.btn_start_bt_u:
                //解除service绑定
                unbindService(conn);
                break;
            case R.id.btn_start_bt_stop:
                stopService(intent2);
                break;
            default:
                break;
        }
    }

    @OnClick(R.id.btn_start_bt_start_3_intents)
    public void onViewClicked() {
        startService(intent3);
    }
}
