package itsen.com.bduidemo.modle.event.nestedscrolling;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.view.NestedScrollingParent;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by 周德森
 * Created time 2017/7/23 19:10
 * Description: NestedScrolling 事件机制
 *           5.0之后的view都实现了该借口，不需要去实现该接口
 * Version: V 1.0
 */

public class NestepLayout extends LinearLayout implements NestedScrollingParent{

    public NestepLayout(Context context) {
        super(context);
    }

    public NestepLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public NestepLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    /**
     * 子View调用startNestedScroll方法  判读父View愿意是否接收后续的滚动事件
     * @param child
     * @param target
     * @param nestedScrollAxes
     * @return 返回true，愿意接受子view滚动事件回调
     */
    @Override
    public boolean onStartNestedScroll(View child, View target, int nestedScrollAxes) {
        //愿意接收后续的滚动事件
        return true;
    }

    /**
     * 子view的dispatchNestedPreScroll调用 把滚动距离传给父View
     * 把已消耗和未消耗的滚动距离传给父View。
     * @param target
     * @param dx
     * @param dy
     * @param consumed consumed[0]表示消耗的水平滚动距离，consumed[1]表示消耗的垂直滚动距离。
     */
    @Override
    public void onNestedPreScroll(View target, int dx, int dy, int[] consumed) {
        super.onNestedPreScroll(target, dx, dy, consumed);
        //这里很重要
    }

    /**
     * 子view的dispatchNestedScroll调用
     * @param target
     * @param dxConsumed
     * @param dyConsumed
     * @param dxUnconsumed
     * @param dyUnconsumed
     */
    @Override
    public void onNestedScroll(View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        super.onNestedScroll(target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
    }



    /**
     * 子View处理Fling事件前，调用dispatchNestedPreFling方法。
     * 如果true，则表示父View处理消耗了该Fling事件，则子View不应该处理该Fling事件。
     * 就会调用父View的onNestedFling方法
     * @param target
     * @param velocityX
     * @param velocityY
     * @return
     */
    @Override
    public boolean onNestedPreFling(View target, float velocityX, float velocityY) {
        //不做拦截 可以传递给子View
        return false;
    }

    /**
     * onNestedFling方法返回true表示父View消耗或处理了Fling事件
     * @param target
     * @param velocityX
     * @param velocityY
     * @param consumed
     * @return
     */
    @Override
    public boolean onNestedFling(View target, float velocityX, float velocityY, boolean consumed) {
        return super.onNestedFling(target, velocityX, velocityY, consumed);
    }

    /**
     * 当子View停止滚动时，调用stopNestedScroll方法。该方法会调用父View的onStopNestedScroll方法。
     * @param child
     */
    @Override
    public void onStopNestedScroll(View child) {
        super.onStopNestedScroll(child);
    }


    /**
     * 以下这两个方法可以忽略了
     */
   /* @Override
    public void onNestedScrollAccepted(View child, View target, int axes) {
        super.onNestedScrollAccepted(child, target, axes);
    }

    @Override
    public int getNestedScrollAxes() {
        return super.getNestedScrollAxes();
    }*/
}
