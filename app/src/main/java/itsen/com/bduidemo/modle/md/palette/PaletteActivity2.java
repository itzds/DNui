package itsen.com.bduidemo.modle.md.palette;


import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class PaletteActivity2 extends BaseAppActivity {

    @BindView(R.id.iv_logo)
    ImageView ivLogo;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.rl_bg)
    RelativeLayout rlyout;

    @Override
    public int getLayoutId() {
        return R.layout.activity_palette2;
    }

    @Override
    public void initData() {
        Intent intent = getIntent();
        Glide.with(this).load(intent.getStringExtra("logoUrl")).into(ivLogo);
        tvName.setText(intent.getStringExtra("name"));
        setToolbarTitle(intent.getStringExtra("name"));
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.finishAfterTransition(PaletteActivity2.this);
            }
        });


        int color = intent.getIntExtra("color",0);
        rlyout.setBackgroundColor(color);
        getToolbar().setBackgroundColor(color);//改变toolbar 的颜色
        if (Build.VERSION.SDK_INT > 21) {
            Window window=getWindow();
            window.setStatusBarColor(colorBurn(color)); //状态栏 颜色加深
            window.setNavigationBarColor(color);//设置导航栏的颜色
        }
    }

    //颜色加深处理
    private int colorBurn(int rgb) {
        //加深颜色
        //int all= (int) (rgb*1.1f);
        int  red=rgb>>16&0xFF;
        int gree=rgb>>8&0xFF;
        int blue=rgb&0xFF;

        red = (int) Math.floor(red * (1 - 0.2));
        gree = (int) Math.floor(gree * (1 - 0.2));
        blue = (int) Math.floor(blue * (1 - 0.2));
        return Color.rgb(red, gree, blue);
    }

}
