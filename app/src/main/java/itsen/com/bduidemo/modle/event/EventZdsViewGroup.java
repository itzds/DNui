package itsen.com.bduidemo.modle.event;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

import itsen.com.bduidemo.lib.tool.LogTool;

/**
 * Created by 周德森
 * Created time 2017/7/9 21:49
 * Description:DNui
 * Version: V 1.0
 *
 * 继承RelativeLayout不用写onLayout方法
 *
 *
 *
 */

public class EventZdsViewGroup extends RelativeLayout {
    private String TAG = "touch";
    public EventZdsViewGroup(Context context) {
        super(context);
    }

    public EventZdsViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    //viewGroup最先执行
    // 1、分发
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        LogTool.e(TAG, "viewGroup  dispatchTouchEvent:" + ev.getAction());
        return super.dispatchTouchEvent(ev);
        //return false;
    }
    //viewGroup 在dispatchTouchEvent后执行
    //2、拦截
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        LogTool.e(TAG, "viewGroup onInterceptTouchEvent:" + ev.getAction());
        //return super.onInterceptTouchEvent(ev);
        return false;
    }
    //viewGroup最后执行
    //3、处理
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        LogTool.e(TAG, "viewGroup onTouchEvent:" + event.getAction());
        //return super.onTouchEvent(event);
        return true;
    }
}
