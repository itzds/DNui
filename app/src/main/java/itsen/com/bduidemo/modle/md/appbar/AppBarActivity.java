package itsen.com.bduidemo.modle.md.appbar;

import android.os.Bundle;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class AppBarActivity extends BaseAppActivity {

    @Override
    public int getLayoutId() {
        return R.layout.activity_app_bar;
    }

    @Override
    public void initData() {

    }
}
