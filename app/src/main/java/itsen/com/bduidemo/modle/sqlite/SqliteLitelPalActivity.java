package itsen.com.bduidemo.modle.sqlite;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.litepal.tablemanager.Connector;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class SqliteLitelPalActivity extends BaseAppActivity {

    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_age)
    EditText etAge;
    @BindView(R.id.et_names)
    EditText etNames;
    @BindView(R.id.tv_result)
    TextView tvResult;
    protected SQLiteDatabase db;

    @Override
    public int getLayoutId() {
        return R.layout.activity_sqlite_litel_pal;
    }

    @Override
    public void initData() {
        db = Connector.getDatabase();//这句话被执行，表就建立好了
    }


    @OnClick({R.id.btn_insert, R.id.btn_update, R.id.btn_delete})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_insert:
                break;
            case R.id.btn_update:
                break;
            case R.id.btn_delete:
                break;
        }
    }

}
