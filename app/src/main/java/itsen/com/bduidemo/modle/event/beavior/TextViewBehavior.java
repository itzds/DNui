package itsen.com.bduidemo.modle.event.beavior;

import android.content.Context;
import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.lib.tool.LogTool;

/**
 * Created by 周德森
 * Created time 2017/8/22 14:03
 * Description:DNui
 * Version: V 1.0
 */

public class TextViewBehavior extends CoordinatorLayout.Behavior<TextView> {
    //必须重写 两个参数的构造  反射时需要用到这个构造函数
    public TextViewBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    //代表寻找被观察View
    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, TextView child, View dependency) {
        LogTool.e("view:"+dependency.toString());
        //告知监听的dependency是Button 并明确id符合要求的btn
        return dependency instanceof Button && (dependency.getId()== R.id.btn2||dependency.getId()== R.id.btn);
    }

    //当 dependency(Button)变化的时候，可以对child(TextView)进行操作
    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, TextView child, View dependency) {
        child.setX(dependency.getX()+200);
        child.setY(dependency.getY()+200);
        child.setText(dependency.getX()+","+dependency.getY());
        return true;
    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, TextView child, View directTargetChild, View target, int nestedScrollAxes) {
        int axis = nestedScrollAxes & ViewCompat.SCROLL_AXIS_VERTICAL;//取方向
        int sdkv = Build.VERSION.SDK_INT;//获取sdk的版本
        return super.onStartNestedScroll(coordinatorLayout, child, directTargetChild, target, nestedScrollAxes);
    }

    @Override
    public boolean onTouchEvent(CoordinatorLayout parent, TextView child, MotionEvent ev) {
        LogTool.e("ev:"+ev.getAction());
        return super.onTouchEvent(parent, child, ev);
    }

}
