package itsen.com.bduidemo.modle.md.palette;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.modle.event.demo.MyRecycleView;


/**
 * Created by 周德森
 * Created time 2017/10/29 12:09
 * Description:DNui
 * Version: V 1.0
 */

public class AdapterGoods extends RecyclerView.Adapter {
    private List<Goods> list;
    private Context context;
    private OnitemClickListener onitemClickListener;

    private static final int TYPE_ITEM   = 0;
    private static final int TYPE_FOOTER = 1;

    //上拉加载更多
    public static final int PULLUP_LOAD_MORE = 0;
    //正在加载中
    public static final int LOADING_MORE     = 1;
    //没有加载更多 隐藏
    public static final int NO_LOAD_MORE     = 2;
    //上拉加载更多状态-默认为0
    private int mLoadMoreStatus = 0;


    public AdapterGoods(List<Goods> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_img,parent,false);
            return new MyViewHolder(view);
        }else if (viewType == TYPE_FOOTER){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_footview,parent,false);
            return new FooterViewHolder(view);
        }
       return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder){
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            Goods goods = list.get(position);
            myViewHolder.tv_name.setText(goods.name);
            myViewHolder.tv_count_compoment.setText(goods.countAndCompoment);
            myViewHolder.tv_price.setText(goods.price);
            Glide.with(context).load(goods.logoUrl).into(myViewHolder.iv_logo);
        }else if (holder instanceof FooterViewHolder){
            FooterViewHolder footerViewHolder = (FooterViewHolder) holder;
            switch (mLoadMoreStatus) {
                case PULLUP_LOAD_MORE:
                    footerViewHolder.textView.setVisibility(View.VISIBLE);
                    footerViewHolder.textView.setText("上拉加载更多...");
                    break;
                case LOADING_MORE:
                    footerViewHolder.textView.setVisibility(View.VISIBLE);
                    footerViewHolder.textView.setText("正加载更多...");
                    break;
                case NO_LOAD_MORE:
                    //隐藏加载更多
                    footerViewHolder.textView.setVisibility(View.GONE);
                    break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position+1 ==getItemCount()){
            return  TYPE_FOOTER;
        }else {
            return TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return list.size()+ 1;//加 1 是为实现上拉时加载footView
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ImageView iv_logo;
        private TextView tv_name;
        private TextView tv_count_compoment;
        private TextView tv_price;
        public MyViewHolder(View itemView) {
            super(itemView);
            iv_logo = (ImageView) itemView.findViewById(R.id.iv_logo);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_count_compoment = (TextView) itemView.findViewById(R.id.tv_count_compoment);
            tv_price = (TextView) itemView.findViewById(R.id.tv_price);
            iv_logo.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onitemClickListener != null){
                Activity activity = (Activity) context;
                ActivityOptionsCompat comapt= ActivityOptionsCompat.
                        makeSceneTransitionAnimation(activity, iv_logo, "logo");
                onitemClickListener.itemClick(v,getAdapterPosition(),comapt);
            }
        }
    }

    /**
     * 定义接口
     */
    public interface OnitemClickListener{
        void itemClick(View v,int position,ActivityOptionsCompat comapt);
    }

    /**
     * 设置接口
     * @param onitemClickListener
     */
    public void setOnitemClickListener(OnitemClickListener onitemClickListener) {
        this.onitemClickListener = onitemClickListener;
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;
        public FooterViewHolder(View itemView) {
            super(itemView);
           textView = (TextView) itemView.findViewById(R.id.tv_add);
        }
    }


    public void AddFooterItem(List<Goods> items) {
        list.addAll(items);
        notifyDataSetChanged();
    }

    /**
     * 更新加载更多状态
     * @param status
     */
    public void changeMoreStatus(int status){
        mLoadMoreStatus=status;
        notifyDataSetChanged();
    }

}
