package itsen.com.bduidemo.modle.dmhx.interf;

/**
 * Created by zds
 * Created time 2017/11/28 19:45
 * Description:DNui
 * Version: V 1.0
 */

public class ClickLinsener {

    MyClikLinsener linsener;

    /**
     * 混淆得保持内部类
     */
    public interface MyClikLinsener{
        void onClick(String s);
    }

    public void setLinsener(MyClikLinsener linsener){
        this.linsener = linsener;
    }

    public void click(MyClikLinsener linsener){
        linsener.onClick("调用");
    }
}
