package itsen.com.bduidemo.modle.net.been;

import java.util.List;

/**
 * @author zds
 * @time 2018年11月8日 下午8:46:52
 * @ClassName 类名称
 * @Description 类描述
 */
public class Customer {

    public String name;
    public Integer id;
    public float count;
    public List<Card> listCards;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public float getCount() {
        return count;
    }

    public void setCount(float count) {
        this.count = count;
    }

    public List<Card> getListCards() {
        return listCards;
    }

    public void setListCards(List<Card> listCards) {
        this.listCards = listCards;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", count=" + count +
                ", listCards=" + listCards +
                '}';
    }
}
