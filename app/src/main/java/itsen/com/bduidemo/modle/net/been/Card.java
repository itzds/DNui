package itsen.com.bduidemo.modle.net.been;


/**
 * @author zds
 * @time 2018年11月8日 下午8:48:47
 * @ClassName 类名称
 * @Description 类描述
 */
public class Card {
    public String date;
    public String cardName;
    public Integer id;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
