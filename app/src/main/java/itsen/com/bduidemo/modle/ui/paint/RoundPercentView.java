package itsen.com.bduidemo.modle.ui.paint;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;

/**
 * Created by 周德森
 * Created time 2017/5/19 10:58
 * Description:DNui
 */

public class RoundPercentView extends View {
    private int mColorBag = 0xFF87CEEB;
    private int mColorP = 0xFF6495ED;
    private int mColorText = 0xFFFFFFFF;//白色

    private Paint mPaintText;
    private Paint mPaint;

    private int textSize = 160;
    private int mRadius;
    private int progress = 80;
    public int curProgress = 0;
    private int max = 100;

    public RoundPercentView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        intiData();
    }

    private void intiData() {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintText = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintText.setColor(mColorText);
        mPaintText.setTextSize(textSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //画背景
        int centerX = getWidth() / 2;
        int centerY = getHeight() / 2;
        mRadius = getWidth() / 3;
        mPaint.setColor(mColorBag);
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(centerX, centerY, mRadius, mPaint);

        //画字
        //Paint.measureText(text) 计算问题的宽度
        //baseline的计算公式：centerY +（FontMetrics.bottom - FontMetrics.top）/2 - FontMetrics.bottom
        String text = curProgress + "";
        Paint.FontMetricsInt fm = mPaintText.getFontMetricsInt();
        canvas.drawText(text, getWidth() / 2 - mPaintText.measureText(text) / 2,
                getHeight() / 2 + (fm.bottom - fm.top) / 2 - fm.bottom, mPaintText);

        //画圆弧
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(mColorP);
        mPaint.setStrokeWidth(10);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mRadius += 5;
        RectF rectF = new RectF(centerX - mRadius, centerY - mRadius, centerX + mRadius, centerY + mRadius);
        canvas.drawArc(rectF, 0, curProgress * 360 / max, false, mPaint);
    }

    public void setProgress(int progress) {
        if (progress < 0) {
            throw new IllegalArgumentException("进度Progress不能小于0");
        }
        if (progress < 100) {
            startAnim(progress);
        }
    }

    private void startAnim(int progress) {
        ValueAnimator animator = ValueAnimator.ofInt(0, progress);
        animator.setDuration(progress * 10);
        // animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                curProgress = (int) animation.getAnimatedValue();
                postInvalidate();
            }
        });
        animator.start();
    }
}
