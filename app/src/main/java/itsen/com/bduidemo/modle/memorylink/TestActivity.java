package itsen.com.bduidemo.modle.memorylink;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class TestActivity extends BaseAppActivity {

    public static AppCompatActivity activity;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test;
    }

    @Override
    public void initData() {

    }

    @OnClick({R.id.btn_one, R.id.btn_two})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_one:
                startActivity(new Intent(this,OneActivity.class));
                break;
            case R.id.btn_two:
                startActivity(new Intent(this,TowActivity.class));
                break;
        }
    }
}
