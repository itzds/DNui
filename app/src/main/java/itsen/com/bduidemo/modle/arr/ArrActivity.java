package itsen.com.bduidemo.modle.arr;

import android.content.Intent;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.apexsoft.ddwtl.ThirdLoginMgr;
import com.apexsoft.ddwtl.WebViewActivity;

import org.json.JSONException;
import org.json.JSONObject;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class ArrActivity extends BaseAppActivity {
    private EditText editText;// 输入的 url  "https://....."
    private boolean isLoginSuccess = false;// true 代表登录成功， false代表未登录
    @Override
    public int getLayoutId() {
        return R.layout.activity_arr;
    }

    @Override
    public void initData() {
        editText = (EditText) findViewById(R.id.main_url);
        // TODO 这是一个进入网厅的按钮
        findViewById(R.id.main_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLoginSuccess){
                    if(!TextUtils.isEmpty(editText.getText().toString())) {
                        //TODO 登录成功 执行回调
                        ThirdLoginMgr.setThirdLogin(new ThirdLoginMgr.ThirdLogin() {
                            @Override
                            public void login() {
                                JSONObject data = new JSONObject();
                                try {
                                    data.put("func", WebViewActivity.SESSION_LOGIN);//固定值，不需要修改
                                    data.put("code", "1");//1 登录成功 0 登录失败
                                    data.put("desc", "登录成功");//登录结果描述
                                    data.put("khh", "000023215186");//客户号（必传）
                                    data.put("duration", "3600");//会话时长（可选）
                                    ThirdLoginMgr.thirdLoginCallback(data.toString());//登录回调
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    //TODO 进入网厅
                    Intent intent = new Intent(ArrActivity.this, WebViewActivity.class);
                    intent.putExtra("url", editText.getText().toString());
                    startActivity(intent);
                }else {
                    //TODO 没有登录 跳到你方的登录页面进行登录  登录成功再执行if语句当中的代码
                    Toast.makeText(ArrActivity.this,"请登录",Toast.LENGTH_LONG).show();
                    if (!isLoginSuccess){
                        isLoginSuccess = true;
                    }
                }
            }
        });
    }

}
