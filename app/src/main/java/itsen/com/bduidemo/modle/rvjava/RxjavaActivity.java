package itsen.com.bduidemo.modle.rvjava;


import android.os.Bundle;
import android.support.annotation.NonNull;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
import itsen.com.bduidemo.lib.tool.LogTool;

/**
 * Rxjava2
 */
public class RxjavaActivity extends BaseAppActivity {

    @Override
    public int getLayoutId() {
        return R.layout.activity_rxjava;
    }

    @Override
    public void initData() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rxjava);
    }


    /**
     * rxjava2 create使用
     */
    private void rxjavaJdsy() {

        //创建一个被观察者
        Observable observable = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> e) throws Exception {
                e.onNext("哈哈");
                e.onNext("呵呵");
                e.onComplete();
                e.onNext("嗯嗯");
            }
        });


        //创建一个光观察者
        Observer<String> observer = new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(String s) {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
    }

    /**
     * rxJava2 map使用
     */
    private void rxJavaMap() {
        Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(ObservableEmitter<Integer> e) throws Exception {
                e.onNext(1);
                e.onNext(2);
                e.onNext(3);
            }
        }).map(new Function<Integer, String>() {

            @Override
            public String apply(Integer integer) throws Exception {
                //对每一个发送过来的数据进行转换进行数据转换
                return "this is:" + integer;
            }
        }).subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {
                LogTool.e(s);
            }
        });
    }


    /**
     * @return
     */
    private Observable<String> getStringObservable() {
        return Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<String> e) throws Exception {
                if (!e.isDisposed()) {
                    e.onNext("A");
                    LogTool.e("String emit : A \n");
                    e.onNext("B");
                    LogTool.e("String emit : B \n");
                    e.onNext("C");
                    LogTool.e("String emit : C \n");
                }
            }
        });
    }

    /**
     * @return
     */
    private Observable<Integer> getIntegerObservable() {
        return Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Integer> e) throws Exception {
                if (!e.isDisposed()) {
                    e.onNext(1);
                    LogTool.e("Integer emit : 1 \n");
                    e.onNext(2);
                    LogTool.e("Integer emit : 2 \n");
                    e.onNext(3);
                    LogTool.e("Integer emit : 3 \n");
                    e.onNext(4);
                    LogTool.e("Integer emit : 4 \n");
                    e.onNext(5);
                    LogTool.e("Integer emit : 5 \n");
                }
            }
        });
    }

    /**
     * zip  专用于合并事件 最终配对出的 Observable 发射事件数目只和少的那个相同。
     */
    private void rxJavaZip() {
        Observable.zip(getStringObservable(), getIntegerObservable(), new BiFunction<String, Integer, String>() {
            @Override
            public String apply(@NonNull String s, @NonNull Integer integer) throws Exception {
                return s + integer;
            }
        }).subscribe(new Consumer<String>() {
            @Override
            public void accept(@NonNull String s) throws Exception {
                LogTool.e("zip : accept : " + s + "\n");
            }
        });
    }


}
