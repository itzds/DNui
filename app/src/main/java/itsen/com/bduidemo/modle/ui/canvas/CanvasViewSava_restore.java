package itsen.com.bduidemo.modle.ui.canvas;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by 周德森
 * Created time 2017/5/25 13:52
 * Description:DNui   知识点： sava  restore
 * Version: V 1.0
 */

public class CanvasViewSava_restore extends View {
    private Paint mPaint;
    private Bitmap mBitmap;
    public CanvasViewSava_restore(Context context) {
        this(context,null);
    }

    public CanvasViewSava_restore(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        /**
         * 1、Matrix又是通过我们设置translate、rotate、scale、skew
         */
        drawRectagle(canvas);//不受影响

        canvas.translate(250,0);// 平移，不是canvas移动了，而是绘图坐标系改变了（Matrix变化），
                                // 在这以下的绘制的图像都将产生影响，之前的不受影响
        drawRectagle(canvas);//受影响

        /*
        * 2、save和restore方法来保存和还原变化操作
        */

        canvas.translate(0,250);
        mPaint.setColor(Color.RED);

        canvas.save();//保存
        canvas.rotate(45);
        drawRectagle(canvas);//画出的正方形旋转45度
        canvas.restore();//回复

        canvas.translate(250,0);//平移，避免重叠，方便对比
        drawRectagle(canvas);//不受影响

    }

    private void drawRectagle(Canvas canvas){
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(5);
        RectF rectF = new RectF(100,100,300,300);
        canvas.drawRect(rectF,mPaint);
    }
}
