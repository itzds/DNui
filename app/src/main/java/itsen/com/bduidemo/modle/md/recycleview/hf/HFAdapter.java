package itsen.com.bduidemo.modle.md.recycleview.hf;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import itsen.com.bduidemo.MyApplicaiton;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.lib.recycleview.BasicRecyViewHolder;
import itsen.com.bduidemo.lib.recycleview.HFSingleTypeRecyAdapter;
import itsen.com.bduidemo.modle.md.palette.Goods;

/**
 * Created by 周德森
 * Created time 2017/10/30 19:10
 * Description:DNui
 * Version: V 1.0
 */

public class HFAdapter extends HFSingleTypeRecyAdapter<Goods,HFAdapter.RecyViewHolder> {


    public HFAdapter(int resId) {
        super(resId);
    }

    @Override
    public RecyViewHolder buildViewHolder(View itemView) {
        return new RecyViewHolder(itemView);
    }

    @Override
    public void bindDataToHolder(RecyViewHolder holder, Goods goods, int position) {
        holder.tv_count_compoment.setText(goods.countAndCompoment);
        holder.tv_name.setText(goods.name);
        holder.tv_price.setText(goods.price);
        Glide.with(MyApplicaiton.getContext()).load(goods.logoUrl).into(holder.iv_logo);
    }

    public static class RecyViewHolder extends BasicRecyViewHolder {

        private ImageView iv_logo;
        private TextView tv_name;
        private TextView tv_count_compoment;
        private TextView tv_price;

        public RecyViewHolder(View itemView) {
            super(itemView);
            iv_logo = (ImageView) itemView.findViewById(R.id.iv_logo);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_count_compoment = (TextView) itemView.findViewById(R.id.tv_count_compoment);
            tv_price = (TextView) itemView.findViewById(R.id.tv_price);
            iv_logo.setOnClickListener(this);
        }
    }
}
