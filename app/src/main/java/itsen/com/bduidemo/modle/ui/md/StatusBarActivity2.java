package itsen.com.bduidemo.modle.ui.md;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.lib.tool.LogTool;
import itsen.com.bduidemo.lib.util.StatusBarUtil;
import itsen.com.bduidemo.lib.util.StatusBarUtils;

public class StatusBarActivity2 extends AppCompatActivity {

    @BindView(R.id.rbg)
    ImageView rbg;
    @BindView(R.id.ly_context)
    LinearLayout lyContext;
    @BindView(R.id.tb_img_bg)
    ImageView tbImgBg;
    @BindView(R.id.title_tool_bar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //API 大于4.4
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //设置状态栏透明
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.activity_status_bar2);
        ButterKnife.bind(this);

        int toolBarHeight = toolbar.getLayoutParams().height;//toolbar的高度
        int statusBarHeit = StatusBarUtil.getStatusBarHeight(this);//statusBar的高度
        int headerBgheight = toolBarHeight + statusBarHeit;//toolbar+statusbar
        ViewGroup.LayoutParams params = tbImgBg.getLayoutParams();
        int mTop = params.height - headerBgheight;

        LogTool.e("headerBgheight:"+headerBgheight+"params.height"+params.height);
        ViewGroup.MarginLayoutParams toolBarImgbgHeadBgParams = (ViewGroup.MarginLayoutParams) tbImgBg.getLayoutParams();
       // toolBarImgbgHeadBgParams.setMargins(0,-mTop,0,0);//toolbal的背景向上移动
        initToolbar();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            //去除默认Title显示
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.icon_back);
        }
        // 手动设置才有效果
        toolbar.setTitleTextAppearance(this, R.style.ToolBar_Title);
        toolbar.setSubtitleTextAppearance(this, R.style.Toolbar_SubTitle);
        toolbar.setTitle("嵌入diam");//标题  没有副标题 标题可能UI要求居中，方案在toolbar中添加一个TextView控件 让他作为标题并设置居中
        toolbar.setSubtitle("我爱唱歌");//副标题
        toolbar.inflateMenu(R.menu.movie_detail);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.actionbar_more));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    // 更多信息
                    case R.id.actionbar_more:
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
    }

}
