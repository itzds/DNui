package itsen.com.bduidemo.modle.ui.path

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import itsen.com.bduidemo.R
import itsen.com.bduidemo.common.activity.BaseAppActivity

class PathActivity : BaseAppActivity() {
    override fun getLayoutId(): Int {
        return R.layout.activity_path;
    }

    override fun initData() {

    }
}
