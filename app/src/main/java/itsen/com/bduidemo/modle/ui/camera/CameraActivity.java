package itsen.com.bduidemo.modle.ui.camera;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class CameraActivity extends BaseAppActivity {

    @Override
    public int getLayoutId() {
        return R.layout.activity_camera;
    }

    @Override
    public void initData() {

    }

}
