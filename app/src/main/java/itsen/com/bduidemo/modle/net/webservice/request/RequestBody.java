package itsen.com.bduidemo.modle.net.webservice.request;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by zhoud
 * Created time  2018/11/10.
 * Description:  请求体
 * Version: V 1.0
 * <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:enc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
 * <soapenv:Body>
 * <getWeatherbyCityName xmlns="http://WebXml.com.cn/">
 * <theCityName>上海</theCityName>
 * </getWeatherbyCityName>
 * </soapenv:Body>
 * </soapenv:Envelope>
 */
@Root(name = "soapenv:Body", strict = false)
public class RequestBody {
    @Element(name = "getWeatherbyCityName", required = false)
    public RequestModel getWeatherbyCityName;
}
