package itsen.com.bduidemo.modle.setting.on;

import org.json.JSONObject;

/**
 * Created by zds
 * Created time 2017/12/20 13:55
 * Description:DNui
 * Version: V 1.0
 */

public abstract class SettingManager {
    private SettingMethodInerface settingMethodInerface;
    public abstract boolean validation(JSONObject obj);
    public  void saveData(JSONObject obj){
        if (settingMethodInerface != null){
            settingMethodInerface.saveData(obj);
        }
    }
    public  void showData(JSONObject obj){
        if (settingMethodInerface != null){
            settingMethodInerface.showData(obj);
        }
    }

    public SettingMethodInerface getSettingMethodInerface() {
        return settingMethodInerface;
    }

    public void setSettingMethodInerface(SettingMethodInerface settingMethodInerface) {
        this.settingMethodInerface = settingMethodInerface;
    }


}
