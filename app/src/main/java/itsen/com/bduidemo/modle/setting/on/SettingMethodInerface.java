package itsen.com.bduidemo.modle.setting.on;

import org.json.JSONObject;

/**
 * Created by zds
 * Created time 2017/12/20 14:05
 * Description:DNui
 * Version: V 1.0
 */

public interface SettingMethodInerface {
      void saveData(JSONObject obj);
      void showData(JSONObject obj);
}
