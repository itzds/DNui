package itsen.com.bduidemo.modle.net.webservice.respone;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by zhoud
 * Created time  2018/11/10.
 * Description:
 * Version: V 1.0
 */
@Root(name = "Body")
public class WeatherResponseBody {
    @Element(name = "getWeatherbyCityNameResponse", required = false)
    public WeatherResponseModel getWeatherbyCityNameResponse;
}
