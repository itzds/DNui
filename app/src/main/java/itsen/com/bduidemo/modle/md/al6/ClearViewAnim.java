package itsen.com.bduidemo.modle.md.al6;

import android.graphics.Canvas;

/**
 * Created by zhoud on 2018/4/29.
 */

public abstract class ClearViewAnim {
    /**
     * @param canvas
     */
    public abstract  void drawState(Canvas canvas);

    /**
     *
     */
    public abstract void startAnim();

    /**
     *
     */
    public abstract void stopAnim( );
}
