package itsen.com.bduidemo.modle.md.al4;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;

import itsen.com.bduidemo.lib.widget.LScrollerView;

/**
 * Created by zds
 * Created time 2018/2/1 15:57
 * Description:DNui
 * Version: V 1.0
 */

public class MyScrollview extends LScrollerView {
    public MyScrollview(Context context) {
        this(context,null);
    }

    public MyScrollview(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public MyScrollview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * 头部视差缩放的图片
     */
    private ImageView mImageview;
    /**
     *最大高度
     */
    private float maxHeight;
    /**
     * ImageView的原始高度
     */
    private int orignalHeight;

    private final static float SCAXL_IMG = 0.2f;


    /**
     * @param scrollX 滑动的距离
     * @param scrollY 滑动的距离
     * @param clampedX
     * @param clampedY 滑动到 顶部 或者 底部 true
     */
    @Override
    protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
        super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
       // Log.e("tag", "deltaY: "+scrollY   +   "  isTouchEvent:"+clampedY);
    }

    /**
     * @param deltaX 继续滑动x方向的距离
     * @param deltaY 继续滑动y方向的距离     负：表示顶部到头   正：表示底部到头
     * @param scrollX
     * @param scrollY
     * @param scrollRangeX
     * @param scrollRangeY
     * @param maxOverScrollX x方向最大可以滚动的距离
     * @param maxOverScrollY y方向最大可以滚动的距离
     * @param isTouchEvent true: 是手指拖动滑动     false:表示fling靠惯性滑动;
     * @return
     */
    @Override
    protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX, int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {
        Log.e("tag", "deltaY: "+deltaY   +   "  isTouchEvent:"+isTouchEvent +"maxHeight:"+maxHeight);
        //向上滑动 且 是手指滑动（不是惯性）
        if (deltaY<0 && isTouchEvent){
            int newHeight = mImageview.getHeight()-deltaY/3;
            if(newHeight>maxHeight){
                newHeight = (int)maxHeight;
            }
            mImageview.getLayoutParams().height = newHeight;
            mImageview.requestLayout();//使ImageView的布局参数生效
        }
        return super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX, scrollRangeY, maxOverScrollX, maxOverScrollY, isTouchEvent);
    }

    public void setmImageview(final ImageView mImageview) {
        this.mImageview = mImageview;
        if (mImageview!=null){
            mImageview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    mImageview.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    //图片的高度
                    orignalHeight = mImageview.getHeight();
                    //图片的实际高度
                    int drawableHeight = mImageview.getDrawable().getIntrinsicHeight();
                    //maxHeight
                    maxHeight = orignalHeight>drawableHeight? orignalHeight*1.5f :drawableHeight+drawableHeight*SCAXL_IMG;
                }
            });
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (ev.getAction()==MotionEvent.ACTION_UP){
            //需要将ImageView的高度缓慢恢复到最初高度
            ValueAnimator animator = ValueAnimator.ofInt(mImageview.getHeight(),orignalHeight);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    //获取动画的值，设置给imageview
                    int animatedValue = (Integer) animator.getAnimatedValue();
                    mImageview.getLayoutParams().height = animatedValue;
                    mImageview.requestLayout();//使ImageView的布局参数生效
                }
            });
            animator.setInterpolator(new OvershootInterpolator(3));//弹性的插值器
            animator.setDuration(350);
            animator.start();
        }
        return super.onTouchEvent(ev);
    }
}
