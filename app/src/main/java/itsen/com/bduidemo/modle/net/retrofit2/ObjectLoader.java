package itsen.com.bduidemo.modle.net.retrofit2;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by zhouds
 * Created time  2018/10/6.
 * Description:
 * Version: V 1.0
 */
public class ObjectLoader {
    /**
     * * @param observable * @param <T> * @return
     */
    protected <T> Observable<T> observe(Observable<T> observable) {
        return observable
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
