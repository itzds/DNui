package itsen.com.bduidemo.modle.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
import itsen.com.bduidemo.lib.tool.ToastTool;
import itsen.com.bduidemo.lib.util.SharedPrefsUtil;
import itsen.com.bduidemo.modle.setting.on.SettingManager;

public class SettingActivity extends BaseAppActivity {
    public final static String SETTING = "setting";
    SettingManager settingManager;
    @BindView(R.id.set_ed_id)
    EditText setEdId;
    @BindView(R.id.set_ed_port)
    EditText setEdPort;

    @Override
    public int getLayoutId() {
        return R.layout.activity_setting;
    }

    @Override
    public void initData() {
        setToolbarTitle(getString(R.string.s_tv_sz));
        settingManager = new SettingMger();
        showRightButton("保存");
        showViewData();
    }
    private void showViewData(){
        setEdId.setText(SharedPrefsUtil.getValue(SETTING,"id","192.160.0.1"));
        setEdPort.setText(SharedPrefsUtil.getValue(SETTING,"port","8080"));
    }

    private void save(){
        SharedPrefsUtil.putValue(SETTING,"id",setEdId.getText().toString());
        SharedPrefsUtil.putValue(SETTING,"port",setEdPort.getText().toString());
    }
    class SettingMger extends SettingManager {

        @Override
        public boolean validation(JSONObject obj) {
            return false;
        }
    }

    @Override
    public void btnClisk(View view) {
        super.btnClisk(view);
        save();
        ToastTool.showLong("保存成功");
    }
}
