package itsen.com.bduidemo.modle.event.demo;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import itsen.com.bduidemo.lib.tool.LogTool;

/**
 * Created by 周德森
 * Created time 2017/10/26 11:52
 * Description:DNui
 * Version: V 1.0
 */

public class MyRecycleView extends RecyclerView {
    private boolean isTouch;

    public MyRecycleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.setOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //LogTool.e("Sy-1::" + recyclerView.canScrollVertically(-1));//-1 在顶部 返回fslse 在底部返回true
                //LogTool.e("Sy+1:" + recyclerView.canScrollVertically(1));  //1  在顶部 返回true  在底部返回false
            }
        });
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        //LogTool.e("Sy:" + this.getScrollY() + "t:" + t + "oldl:" + oldl + "oldt" + oldt);
        if (isTouch) {
            isTouch = false;
        }
        if (l == 0) {

        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        //return false;
       // LogTool.e("Sy:" + this.getScrollY());
        /*if (e.getAction() == MotionEvent.ACTION_MOVE){
            if (this.canScrollVertically(1)){
                return false;
            }
        }*/
        return super.onTouchEvent(e);
    }

    @Override
    public boolean canScrollVertically(int direction) {
        LogTool.e("direction"+direction);
        return super.canScrollVertically(direction);
    }
}
