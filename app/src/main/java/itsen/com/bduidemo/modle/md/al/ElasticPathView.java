package itsen.com.bduidemo.modle.md.al;

import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.support.annotation.Nullable;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;

/**
 * Created by zds
 * Created time 2018/2/2 10:56
 * Description: 弹性View
 * Version: V 1.0
 */

public class ElasticPathView extends View {
    /**
     * 背景颜色
     */
    private final static int COLOR_BG = 0xffededed;
    /**
     * 控制点1
     */
    private Point mPoint1;
    /**
     * 控制点2
     */
    private Point mPoint2;
    /**
     *  view 的高度
     */
    private int mViewHeight;
    /**
     *  view 的宽度
     */
    private int mViewWdith;
    /**
     * path上升的  mViewHeight 变成 0 最高
     */
    private float mDy;

    private Paint mPaint;

    private Path mPath;

    public ElasticPathView(Context context) {
        this(context,null);
    }

    public ElasticPathView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public ElasticPathView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mViewHeight = getMeasuredHeight();
        mViewWdith = getMeasuredWidth();
        mDy = mViewHeight;
    }

    private void init(){
        mPath = new Path();
        mPaint = new Paint();

        mPaint.setColor(COLOR_BG);
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);
    }



    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawElasticPath(canvas);
    }

    private void drawElasticPath(Canvas canvas){
        initPath();
        canvas.drawPath(mPath,mPaint);
    }

    private void initPath(){
        mPath.reset();
        mPath.moveTo(0,mViewHeight);
        mPath.quadTo(mViewWdith/2,mDy,mViewWdith,mViewHeight);
        mPath.close();
    }

    public void animPath(){
        ValueAnimator animator = ValueAnimator.ofFloat(mViewHeight,0,mViewHeight);
        animator.setDuration(800);
        animator.setInterpolator(new FastOutSlowInInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
               // Log.e("tag","animation:"+animation.getAnimatedValue());
                mDy = (float)animation.getAnimatedValue();
                postInvalidate();
            }
        });
        animator.start();
    }
}
