package itsen.com.bduidemo.modle.study.intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class IntentActivity extends BaseAppActivity {

    @Override
    public int getLayoutId() {
        return R.layout.activity_intent;
    }

    @Override
    public void initData() {

    }

}
