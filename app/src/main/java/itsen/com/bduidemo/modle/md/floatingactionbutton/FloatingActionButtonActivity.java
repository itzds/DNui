package itsen.com.bduidemo.modle.md.floatingactionbutton;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;

public class FloatingActionButtonActivity extends BaseAppActivity {

    @BindView(R.id.fab_button)
    FloatingActionButton fabButton;
    @BindView(R.id.coorlayout)
    CoordinatorLayout coordinatorLayout;

    @Override
    public int getLayoutId() {
        return R.layout.activity_floating_action_button;
    }

    @Override
    public void initData() {
        //得到Snackbar对象
        final Snackbar snackbar = Snackbar.make(coordinatorLayout, "我是Snackbar", Snackbar.LENGTH_LONG);
        //设置Snackbar背景
        snackbar.getView().setBackgroundResource(R.color.colorPrimary);
        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.show();
                //显示带Action的Snackbar
                snackbar.setAction("取消", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //关闭Snackbar
                        snackbar.dismiss();
                    }
                });
            }
        });

    }

}
