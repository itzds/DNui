package itsen.com.bduidemo.modle.net.been;

import java.util.Map;

/**
 * @author zds
 * @time 2018年1月2日 下午10:28:18
 * @ClassName 类名称
 * @Description 类描述
 */
public class JsonRespone {
	public Integer code;//相应code
	public Integer status;//0 失败 1成功
	public String msg;//信息
	public Map<String, Object> body;
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Map<String, Object> getBody() {
		return body;
	}
	public void setBody(Map<String, Object> body) {
		this.body = body;
	}
	
}
