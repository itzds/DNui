package itsen.com.bduidemo.modle.ui.canvas;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by 周德森
 * Created time 2017/5/26 11:07
 * Description:DNui
 * Version: V 1.0
 */

public class CanvasViewClip extends View{
    public CanvasViewClip(Context context) {
        this(context,null);
    }

    public CanvasViewClip(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        RectF rectF = new RectF(100,100,400,400);
        canvas.clipRect(rectF);
        canvas.drawColor(Color.BLUE);
    }
}
