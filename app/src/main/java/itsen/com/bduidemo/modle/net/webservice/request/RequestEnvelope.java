package itsen.com.bduidemo.modle.net.webservice.request;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;

/**
 * Created by zhoud
 * Created time  2018/11/10.
 * Description: 定义soap版本、根标签
 * Version: V 1.0
 *
 * <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:enc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
 * <soapenv:Body>
 * <getWeatherbyCityName xmlns="http://WebXml.com.cn/">
 * <theCityName>上海</theCityName>
 * </getWeatherbyCityName>
 * </soapenv:Body>
 * </soapenv:Envelope>
 */
@Root(name = "soapenv:Envelope")
@NamespaceList({
        @Namespace(reference = "http://www.w3.org/2001/XMLSchema-instance", prefix = "xsi"),
        @Namespace(reference = "http://www.w3.org/2001/XMLSchema", prefix = "xsd"),
        @Namespace(reference = "http://schemas.xmlsoap.org/soap/encoding/", prefix = "enc"),
        @Namespace(reference = "http://schemas.xmlsoap.org/soap/envelope/", prefix = "soapenv")
})
public class RequestEnvelope {
    @Element(name = "soapenv:Body", required = false)
    public RequestBody body;
}
