package itsen.com.bduidemo.modle.ui.animtion;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;

import itsen.com.bduidemo.R;


public class ContentView extends AppCompatImageView {
    public ContentView(Context context) {
        super(context);
        setImageResource(R.drawable.ic_anim_content);
    }
}
