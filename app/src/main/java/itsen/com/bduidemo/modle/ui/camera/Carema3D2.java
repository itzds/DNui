package itsen.com.bduidemo.modle.ui.camera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import itsen.com.bduidemo.R;

/**
 * Created by 周德森
 * Created time 2017/10/28 20:53
 * Description:DNui
 * Version: V 1.0
 */

public class Carema3D2 extends View {
    private Paint mPaint;
    private Camera mCamera;
    private Matrix mMatrix;
    private Bitmap mBitmap;
    public Carema3D2(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(Color.BLUE);
        mMatrix = new Matrix();
        mCamera = new Camera();
        mBitmap = BitmapFactory.decodeResource(getResources(),R.drawable.ic_music_hread);
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mMatrix.reset();

        mCamera.save();
        mCamera.rotateX(45);// 绕X轴旋转 45度
        mCamera.getMatrix(mMatrix);
        mCamera.restore();

        //旋转中心
        mMatrix.preTranslate(mBitmap.getWidth()/2,0);
        mMatrix.postTranslate(-mBitmap.getWidth()/2,0);

       canvas.drawBitmap(mBitmap,mMatrix,mPaint);


    }
}
