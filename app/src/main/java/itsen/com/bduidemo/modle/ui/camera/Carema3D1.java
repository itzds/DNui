package itsen.com.bduidemo.modle.ui.camera;

import android.content.Context;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by 周德森
 * Created time 2017/10/28 20:53
 * Description:DNui
 * Version: V 1.0
 */

public class Carema3D1 extends View {
    private Paint mPaint;
    private Camera mCamera;
    private Matrix mMatrix;
    public Carema3D1(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(Color.BLUE);
        mMatrix = new Matrix();
        mCamera = new Camera();
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mMatrix.reset();

        mCamera.save();//==开始3D
        mCamera.translate(10, 50, -180); //  在3D camera中  x+ ：向右    y+：向上  z+  向屏幕里面（变小）
        mCamera.getMatrix(mMatrix);
        canvas.concat(mMatrix);//画布与矩阵关联
        mCamera.restore();//==回复

        canvas.drawCircle(60, 60, 60, mPaint);


    }
}
