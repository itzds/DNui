package itsen.com.bduidemo.modle.net.webservice.respone;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by zhoud
 * Created time  2018/11/10.
 * Description:
 * Version: V 1.0
 */

@Root(name = "getWeatherbyCityNameResponse")
@Namespace(reference = "http://WebXml.com.cn/")
public class WeatherResponseModel {
    @ElementList(name = "getWeatherbyCityNameResult")
    public List<String> result;
}
