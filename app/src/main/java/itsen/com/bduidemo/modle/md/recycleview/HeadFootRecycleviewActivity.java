package itsen.com.bduidemo.modle.md.recycleview;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
import itsen.com.bduidemo.lib.recycleview.BasicRecyViewHolder;
import itsen.com.bduidemo.lib.recycleview.HFLineVerComDecoration;
import itsen.com.bduidemo.lib.recycleview.NestedRefreshLayout;
import itsen.com.bduidemo.lib.recycleview.RecycleScrollListener;
import itsen.com.bduidemo.lib.util.DensityUtils;
import itsen.com.bduidemo.modle.md.DataUtils;
import itsen.com.bduidemo.modle.md.recycleview.hf.HFAdapter;

public class HeadFootRecycleviewActivity extends BaseAppActivity implements BasicRecyViewHolder.OnItemClickListener{

    @BindView(R.id.rv_list)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    NestedRefreshLayout refreshLayout;

    View loadingView;
    View nodataView;
    View topView;
    HFAdapter adapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_head_foot_recycleview;
    }

    @Override
    public void initData() {
        loadingView = getLayoutInflater().inflate(R.layout.item_loadingview,null);
        nodataView = getLayoutInflater().inflate(R.layout.item_footview_nodata1, null);
        if (adapter == null){
            adapter = new HFAdapter(R.layout.item_liner_img);
            adapter.setHeadView(topView);
            adapter.setFootView(loadingView);
            adapter.setItemClickListener(this);
            adapter.addSubViewListener(R.id.iv_logo, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            recyclerView.setAdapter(adapter);
            recyclerView.addItemDecoration(new HFLineVerComDecoration(1, Color.parseColor("#ededed")));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            adapter.refreshDatas(DataUtils.getListGoods());
            refreshLayout.setOnRefreshListener(listener);
            recyclerView.addOnScrollListener(srcollListener);
        }
    }

    @Override
    public void OnItemClick(View v, int adapterPosition) {

    }

    public  NestedRefreshLayout.OnRefreshListener listener = new NestedRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            refreshFinish();
        }
    };

    public void refreshFinish(){
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshLayout.refreshFinish();
                    }
                });
            }
        },2000);
    }

    public RecycleScrollListener srcollListener = new RecycleScrollListener() {
        @Override
        public void loadMore() {
            if (adapter.getDatas().size() >20) {
                adapter.updateFootView(nodataView);
            } else {
                adapter.appendDatas(DataUtils.getListGoods());
                handler.sendEmptyMessageDelayed(0,1000);
            }
        }


        @Override
        public void refresh() {

        }
    };

    android.os.Handler handler=new android.os.Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            srcollListener.finished();
        }
    };
}
