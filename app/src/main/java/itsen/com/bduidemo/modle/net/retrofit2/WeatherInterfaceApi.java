package itsen.com.bduidemo.modle.net.retrofit2;

import itsen.com.bduidemo.modle.net.webservice.request.RequestEnvelope;
import itsen.com.bduidemo.modle.net.webservice.respone.ResponseEnvelope;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by zhoud
 * Created time  2018/11/10.
 * Description:
 * Version: V 1.0
 */
public interface WeatherInterfaceApi {
    @Headers({"Content-Type: text/xml;charset=UTF-8", "SOAPAction: http://WebXml.com.cn/getWeatherbyCityName"})//请求的Action，类似于方法名
    @POST("WeatherWebService.asmx")
    Call<ResponseEnvelope> getWeatherbyCityName(@Body RequestEnvelope requestEnvelope);
}
