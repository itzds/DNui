package itsen.com.bduidemo.lib.net;

import android.os.Handler;
import android.os.Message;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;


import itsen.com.bduidemo.lib.thread.ThreadPool;
import itsen.com.bduidemo.lib.tool.LogTool;

/**
 * Created by 周德森
 * Created time 2017/7/20 15:22
 * Description:网络请求
 * Version: V 1.0
 */

public class HttpClinetUtils {
    public final static int LONG_TIMEOUT = 8000;
    public final static int SUCCESS_CODE = 0;
    public final static int SUCCESS_CODE1 = 1;
    public final static int SUCCESS_CODE2 = 2;
    public final static int SUCCESS_CODE3 = 3;
    public final static int SUCCESS_CODE4 = 4;
    public final static int ERROR_CODE = 99;
    private static final String TAG = "HttpClinetUtils";

    /**
     * @param url
     * @param handler
     * @param responeCode
     * @param errCode
     */
    public static void get(final String url, final Handler handler, final int responeCode, final int errCode){
        ThreadPool.getmFixedThreadPool().submit(new Runnable() {
            @Override
            public void run() {
                LogTool.e(url);
                StringBuffer buffer = null;
                String result = null;
                //这里根据需要
                try {
                    /*FileOutputStream fos=new FileOutputStream(new File(SDUtil.cachePath+"/log.txt"),true);
                    fos.write(("时间:" + new Date() + "    请求:" + url).getBytes());
                    fos.flush();
                    fos.close();*/
                }catch (Exception e){
                    e.printStackTrace();
                }

                try {
                    URL mUrl = new URL(url);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) mUrl.openConnection();
                    httpURLConnection.setConnectTimeout(LONG_TIMEOUT);
                    httpURLConnection.setDoInput(true);
                    httpURLConnection.setRequestMethod("GET");
                    if(httpURLConnection.getResponseCode()==HttpURLConnection.HTTP_OK) {
                        InputStream inputStream = httpURLConnection.getInputStream();
                        buffer = new StringBuffer();
                        byte[] b = new byte[1024];
                        int len = 0;
                        while ((len = inputStream.read(b)) != -1) {
                            buffer.append(new String(b, 0, len));
                        }
                        result = buffer.toString();
                        //这里根据需要记录返回的请求值
                        Message msg=handler.obtainMessage();
                        msg.what=responeCode;
                        msg.obj=result;
                        handler.sendMessage(msg);
                    }else {
                        handler.sendEmptyMessage(errCode);
                    }
                }catch (Exception e){
                    handler.sendEmptyMessage(errCode);
                    e.printStackTrace();
                }
            }
        });
    }
    public static void post(final String url, final Handler handler, final int responeCode, final int errCode){

    }
}
