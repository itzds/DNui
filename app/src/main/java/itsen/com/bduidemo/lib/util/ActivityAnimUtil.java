package itsen.com.bduidemo.lib.util;

import android.app.Activity;

import itsen.com.bduidemo.R;


/**
 *
 * Description:  类描述
 * Author:  周德森
 * CreateAt:  2017/2/25 0025  下午 3:55
 * Conpany:
 * Copyright:  2016 www.wwhqj.com  Inc. All rights reserved.
 */
public class ActivityAnimUtil {
   public static void startAnim(Activity activity){
       activity.overridePendingTransition(R.anim.zoomin, R.anim.zoomout);
   }
}
