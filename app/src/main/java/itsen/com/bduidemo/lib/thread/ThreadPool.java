package itsen.com.bduidemo.lib.thread;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by 周德森
 * Created time 2017/7/19 16:30
 * Description:线程池
 * Version: V 1.0
 */

public class ThreadPool {

    /**
     * 线程数
     */
    private  final static int POOL_SIZE = 16;
    private static ExecutorService mFixedThreadPool;//固定大小线程池

    public static ExecutorService getmFixedThreadPool(){
        if (mFixedThreadPool==null){
            mFixedThreadPool = Executors.newFixedThreadPool(POOL_SIZE);
        }
        return mFixedThreadPool;
    }

    
}
