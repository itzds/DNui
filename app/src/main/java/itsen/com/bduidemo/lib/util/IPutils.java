package itsen.com.bduidemo.lib.util;

/**
 * Created by zhoud on 2018/1/5.
 */

public class IPutils {
    public final static String SETTING = "setting";
    public static String  getIP(){
       return
               "http://"+ SharedPrefsUtil.getValue(SETTING,"id","192.168.0.101")
               +":"
               +SharedPrefsUtil.getValue(SETTING,"port","8080")
               +"/";
    }

    public static String  getIP2(){
        return
                "https://"+ SharedPrefsUtil.getValue(SETTING,"id","192.168.0.101")
                        +"/";

    }
}
