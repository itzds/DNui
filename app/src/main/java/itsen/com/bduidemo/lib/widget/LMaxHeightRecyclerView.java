package itsen.com.bduidemo.lib.widget;


import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;


import itsen.com.bduidemo.lib.util.DensityUtils;

public class LMaxHeightRecyclerView extends RecyclerView {
	private int max_height = 300;

	public LMaxHeightRecyclerView(Context context) {
		super(context);
	}

	public LMaxHeightRecyclerView(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		max_height = DensityUtils.dp2px( max_height);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		if (getChildCount() > 0) {
			int height;
			View child = getChildAt(0);
			RecyclerView.LayoutParams params = (LayoutParams) child.getLayoutParams();
			child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
			//计算出总高度
			int max = getAdapter().getItemCount() * (child.getMeasuredHeight() + getPaddingTop() + getPaddingBottom() + params.topMargin + params
					.bottomMargin);
			//取最小高度，限制最大高度不能大于 max_height
			height = Math.min(max, max_height);
			setMeasuredDimension(widthMeasureSpec, height);
		} else {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}
	}
}
