package itsen.com.bduidemo.lib.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.lib.tool.LogTool;

/**
 * Created by 周德森
 * Created time 2017/5/18 21:01
 * Description:自定义进度条，支持水平和竖直
 * version:v1.0  完善后加动画改变透明度渐渐消失
 */

public class ProgressView extends View {
    private int mMaxnum = 100;
    private int mPernum;
    private boolean isHorizotal;
    private int mBackgroundColor;
    private int mDefaultcolr = 0xaa00ff00;

    private int currentWidth;
    private int currentHeight;
    private int viewHeight;
    private int viewWidth;

    private Paint mPaint;

    public ProgressView(Context context) {
        this(context, null);
    }

    public ProgressView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProgressView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inti(context, attrs);
    }

    private void inti(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ProgressView);
            isHorizotal = typedArray.getBoolean(R.styleable.ProgressView_horizotal, true);
            mBackgroundColor = typedArray.getColor(R.styleable.ProgressView_backgroundColor, mDefaultcolr);
            typedArray.recycle();
        }
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setAntiAlias(true);
        mPaint.setColor(mBackgroundColor);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //就不测了 因为是精确的值和match_parent
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        LogTool.e("w", heightSize + "widthSize" + widthSize);
        viewHeight = heightSize;
        viewWidth = widthSize;
        if (isHorizotal) {
            currentHeight = viewHeight;
            mPernum = viewWidth / mMaxnum;
        } else {
            mPernum = viewHeight / mMaxnum;
            currentWidth = viewWidth;
        }
        setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(0, 0, currentWidth, currentHeight, mPaint);
    }

    //设置最大进度条
    public void setmMaxnum(int maxnum) {
        this.mMaxnum = maxnum;
        if (isHorizotal) {
            mPernum = viewWidth / mMaxnum;
        } else {
            mPernum = viewHeight / mMaxnum;
        }
    }

    //设置当前进度
    public void setProgressValue(int value) {
        if (value == 0) {
            this.setVisibility(GONE);
        } else {
            this.setVisibility(VISIBLE);
        }
        if (isHorizotal) {
            currentWidth = mPernum * value;
        } else {
            currentHeight = mPernum * value;
        }
        postInvalidate();
    }
}
