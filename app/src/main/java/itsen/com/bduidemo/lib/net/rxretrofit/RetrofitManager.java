package itsen.com.bduidemo.lib.net.rxretrofit;

import android.text.TextUtils;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import itsen.com.bduidemo.MyApplicaiton;
import itsen.com.bduidemo.lib.net.SSLHelper;
import itsen.com.bduidemo.lib.net.UnSafeHostnameVerifier;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by zhoud
 * Created time  2018/11/4.
 * Description:
 * Version: V 1.0
 */
public class RetrofitManager {
    /**
     *  baseUrl  在application中赋值
     */
    public static String baseUrl;
    /**
     * token
     */
    public static String userToken = "4234345dfgd754df";
    /**
     * 超时时间
     */
    private static final int DEFAULT_TIME_OUT = 15;
    private static RetrofitManager mRetrofitManager;
    private Retrofit mRetrofit;
    private static HttpLoggingInterceptor loggingInterceptor;
    public static OkHttpClient okHttpClient;

    //静态块,获取OkHttpClient对象
    static {
        getOkHttpClient();
    }

    private RetrofitManager() {
        if (TextUtils.isEmpty(baseUrl)){
            new Throwable("baseUrl is Empty").printStackTrace();
        }
        mRetrofit = new Retrofit.Builder()
                .baseUrl(baseUrl+"ForAnd/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public static synchronized RetrofitManager getInstance() {
        if (mRetrofitManager == null) {
            synchronized (RetrofitManager.class) {
                if (mRetrofitManager == null) {
                    mRetrofitManager = new RetrofitManager();
                }
            }
        }
        return mRetrofitManager;
    }


    /**
     *  获取单例OkHttpClient
     *
     * @return
     */
    @SuppressWarnings("deprecated")
    public static OkHttpClient getOkHttpClient() {
        //添加公共参数拦截器
        HttpCommonInterceptor interceptor = new HttpCommonInterceptor.Builder()
                .addHeaderParams("paltform", "android")
                .addHeaderParams("userToken", userToken)
                .build();

        //日志
        loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                //打印retrofit日志
                Log.i("RetrofitLog", "retrofitBack = " + message);
            }
        });
        //日志等级
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        if (okHttpClient == null) {
            synchronized (OkHttpClient.class) {
                if (okHttpClient == null) {
                    okHttpClient = new OkHttpClient.Builder()
                            .sslSocketFactory(SSLHelper.getSSLCertifcation(MyApplicaiton.getContext()))
                            .hostnameVerifier(new UnSafeHostnameVerifier())
                            //打印拦截器日志
                            .addNetworkInterceptor(loggingInterceptor)
                            //设置连接超时时间
                            .connectTimeout(DEFAULT_TIME_OUT, TimeUnit.SECONDS)
                            //设置读取超时时间
                            .readTimeout(DEFAULT_TIME_OUT, TimeUnit.SECONDS)
                            // 设置写入超时时间
                            .writeTimeout(DEFAULT_TIME_OUT, TimeUnit.SECONDS)
                            .build();
                }
            }
        }
        return okHttpClient;
    }

    /**
     * 创建相应的服务接口
     */
    public <T> T create(Class<T> reqServer) {
        return mRetrofit.create(reqServer);
    }
}
