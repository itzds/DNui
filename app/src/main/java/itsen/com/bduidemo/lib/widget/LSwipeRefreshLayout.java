package itsen.com.bduidemo.lib.widget;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;

import itsen.com.bduidemo.R;

/**
 * Created by 周德森
 * Created time 2017/10/29 19:31
 * Description:DNui
 * Version: V 1.0
 */

public class LSwipeRefreshLayout extends SwipeRefreshLayout {
    public LSwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        this.setColorSchemeResources(
                R.color.swipe_color_1,
                R.color.swipe_color_2);
        this.setSize(60);
        this.setProgressViewEndTarget(true, 160);
    }
}
