package itsen.com.bduidemo.lib.util;

import android.os.Build;

/**
 * Created by zhoud
 * Created time  2018/9/9.
 * Description:
 * Version: V 1.0
 */
public class SystemInfoUtil {
    /**
     * 主板
     *
     * @return
     */
    public static String getBoara() {
        return Build.BOARD;
    }

    /**
     * 系统定制商
     * @return
     */
    public static String getBrand() {
        return Build.BRAND;
    }

    /**
     * 设备参数
     * @return
     */
    public static String getDevice() {
        return Build.DEVICE;
    }

    /**
     * 显示屏幕参数
     * @return
     */
    public static String getDisplay() {
        return Build.DISPLAY;
    }

    /**
     * 手机产品名
     * @return
     */
    public static String getProduct() {
        return Build.PRODUCT;
    }

    /**
     * 获取系统版本号
     * @return
     */
    public static int getSDK() {
        return Build.VERSION.SDK_INT;
    }

    /**
     * Android 系统信息获取 简单的举个例子
     */

    public static String getOsVersion(){
        return System.getProperty("os.version");
    }


}
