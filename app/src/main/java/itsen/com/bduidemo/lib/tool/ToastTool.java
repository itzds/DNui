package itsen.com.bduidemo.lib.tool;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

import itsen.com.bduidemo.MyApplicaiton;

/**
 * Created by 周德森
 * Created time 2017/10/28 15:21
 * Description:DNui
 * Version: V 1.0
 * http://blog.csdn.net/wwt831208/article/details/54729989
 */

public class ToastTool {
    private static boolean isShow = true;//默认显示
    private static Toast mToast = null;//全局唯一的Toast
    private static Context mContext = MyApplicaiton.getContext();//全局上下文

    private ToastTool() {
        throw new UnsupportedOperationException("不能被实例化");
    }

    /**
     * 短时间显示Toast
     *
     * @param message
     */
    public static void showShort(CharSequence message) {
        if (isShow){
            if (mToast == null) {
                mToast = Toast.makeText(mContext, message, Toast.LENGTH_SHORT);
            } else {
                mToast.setText(message);
            }
            mToast.show();
        }
    }

    /**
     * 短时间显示Toast
     *
     * @param resId 资源ID:getResources().getString(R.string.xxxxxx);
     */
    public static void showShort(int resId) {
        if (isShow){
            if (mToast == null) {
                mToast = Toast.makeText(mContext, resId, Toast.LENGTH_SHORT);
            } else {
                mToast.setText(resId);
            }
            mToast.show();
        }
    }

    /**
     * 长时间显示Toast
     *
     * @param message
     */
    public static void showLong(CharSequence message) {
        if (isShow){
            if (mToast == null) {
                mToast = Toast.makeText(mContext, message, Toast.LENGTH_LONG);
            } else {
                mToast.setText(message);
            }
            mToast.show();
        }
    }
}
