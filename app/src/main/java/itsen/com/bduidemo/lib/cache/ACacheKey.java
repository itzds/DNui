package itsen.com.bduidemo.lib.cache;

/**
 * Created by 周德森
 * Created time 2017/7/18 17:09
 * Description:DNui
 * Version: V 1.0
 */

public class ACacheKey {
    public final static String KEY_LOGIN_STATE = "Login_state";
    public final static String KEY_USER_ID = "User_id";
}
