package itsen.com.bduidemo.lib.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * Created by 周德森
 * Created time 2017/7/22 12:02
 * Description:嵌套ListView 显示完整高度
 * Version: V 1.0
 */

public class LListView extends ListView {
    public LListView(Context context) {
        super(context);
    }

    public LListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);

        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
