package itsen.com.bduidemo.lib.tool;

import android.util.Log;
import itsen.com.bduidemo.config.Config;

/**
 * Created by zhoudesen
 * Created time 2017/5/18 17:10
 * Description:DNui
 */

public class LogTool {
    public static void e(String tag,String contxt){
        if(Config.debug){
            Log.e(tag,contxt);
        }
    }

    public static void e(String contxt){
        if(Config.debug){
            Log.e(Config.TAG_E,contxt);
        }
    }

    public static void v(String tag,String contxt){
        if(Config.debug){
            Log.v(tag,contxt);
        }
    }
    public static void d(String tag,String contxt){
        if(Config.debug){
            Log.d(tag,contxt);
        }
    }
}
