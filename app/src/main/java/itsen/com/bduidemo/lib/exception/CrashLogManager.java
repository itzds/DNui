package itsen.com.bduidemo.lib.exception;

/**
 * Created by 周德森
 * Created time 2017/7/13 14:23
 * Description:DNui
 * Version: V 1.0
 */

public class CrashLogManager implements Thread.UncaughtExceptionHandler {

    /**
     * 当UncaughtException发生时会转入该函数来处理
     */
    @Override
    public void uncaughtException(Thread thread, Throwable ex) {

    }
}
