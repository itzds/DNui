package itsen.com.bduidemo.lib;

import android.widget.LinearLayout;
import android.widget.TextView;

import itsen.com.bduidemo.R;
import itsen.com.bduidemo.common.activity.BaseAppActivity;
import itsen.com.bduidemo.lib.widget.LScrollerView;

public class LibTestActivity extends BaseAppActivity {


    @Override
    public int getLayoutId() {
        return R.layout.activity_lib_test;
    }

    @Override
    public void initData() {
        LinearLayout lyLibTest = (LinearLayout) findViewById(R.id.ly_lib_test);
        LScrollerView scrollerView = (LScrollerView) findViewById(R.id.sv_context);
        TextView textView1 = (TextView) findViewById(R.id.tv_1);
        TextView textView2 = (TextView) findViewById(R.id.tv_2);
        TextView textView3 = (TextView) findViewById(R.id.tv_3);
    }

}
