package itsen.com.bduidemo.lib.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

import itsen.com.bduidemo.lib.tool.LogTool;

/**
 * Created by 周德森
 * Created time 2017/7/22 11:54
 * Description:DNui
 * Version: V 1.0
 */

public class LScrollerView extends ScrollView {
    public LScrollerView(Context context) {
        super(context);
    }

    public LScrollerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LScrollerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * 设置滚动接口
     *
     * @param onScrollListener
     */
    public void setOnScrollListener(OnScrollListener onScrollListener) {
        this.onScrollListener = onScrollListener;
    }


    /**
     * @param l
     * @param t  往上移动的距离
     * @param oldl
     * @param oldt
     */
    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        LogTool.d(VIEW_LOG_TAG, "l=" + l + " t=" + t + " oldl=" + oldl + " oldt="
                + oldt);
        if (onScrollListener != null) {
            onScrollListener.onScroll(t);
        }
        LogTool.d(VIEW_LOG_TAG, ""+getScrollY());
    }

    /**
     * 滚动的回调接口
     */
    public interface OnScrollListener {
        /**
         * 回调方法， 返回MyScrollView滑动的Y方向距离
         *
         * @param scrollY
         *            、
         */
        public void onScroll(int scrollY);
    }

    private OnScrollListener onScrollListener;
}
