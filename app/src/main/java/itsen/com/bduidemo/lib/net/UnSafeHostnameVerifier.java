package itsen.com.bduidemo.lib.net;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import itsen.com.bduidemo.lib.tool.LogTool;

/**
 * Created by zhoud
 * Created time  2018/03/27.
 * Description:
 * Version: V 1.0
 */
public class UnSafeHostnameVerifier implements HostnameVerifier {
    @Override
    public boolean verify(String hostname, SSLSession session) {
        LogTool.e("hostname:"+hostname);
        return true;
    }
}
