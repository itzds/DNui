package itsen.com.bduidemo.common.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextPaint;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import itsen.com.bduidemo.R;
import itsen.com.bduidemo.lib.tool.LogTool;
import itsen.com.bduidemo.lib.util.ActivityAnimUtil;

/**
 * Created by zhoudesen
 * Created time 2017/5/18 11:09
 * Description:基类
 */

public abstract class BaseAppActivity extends AppCompatActivity {

    public abstract int getLayoutId();

    public abstract void initData();
    private Toolbar toolbar;
    private Intent intent;
    private Unbinder unbinder;
    public final static String KEY = "baseIntent";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        LogTool.e("onCreate");
       // DataBindingUtil.setContentView(this,getLayoutId());
        setContentView(getLayoutId());
        intent = getIntent();
        unbinder = ButterKnife.bind(this);
        initToolbar();
        setToolbarTitle(getExtraString(KEY));
        initData();
    }

    /**
     * toolbar初始化
     */
    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_c_ly);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    ActivityAnimUtil.startAnim(BaseAppActivity.this);
                }
            });
        }
    }

    /**
     * 设置标题
     *
     * @param title
     */
    public void setToolbarTitle(String title) {
        if (toolbar != null) {
            TextView textView = (TextView) toolbar.findViewById(R.id.tb_title);
            if (textView!=null){
                TextPaint tp = textView.getPaint();
                tp.setFakeBoldText(true);
                textView.setText(title);
                textView.setSelected(true);
            }
        }
    }
    /**
     * 显示左边
     *
     */
    public void showRightButton(String text){
        Button button = (Button) toolbar.findViewById(R.id.btn_right);
        if (button !=null){
            button.setVisibility(View.VISIBLE);
            button.setText(text);
        }
    }

    /**
     * 隐藏返回按钮
     */
    public void setToolbarNavGone() {
        if (toolbar != null) {
            toolbar.setNavigationIcon(null);
        }
    }

    /**
     *
     * @return  toolbar
     */
    public Toolbar getToolbar(){
        return toolbar;
    }

    /**
     * 获取value
     *
     * @param key
     * @return
     */
    public String getExtraString(String key) {
        if (intent != null) {
            if(intent.getStringExtra(key)==null){
                new IllegalAccessError("无法获取key="+key+"的对应值");
            }
            return intent.getStringExtra(key);
        }
        return "";
    }

    @Override
    protected void onStop() {
        super.onStop();
        LogTool.e("onStop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        LogTool.e("onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogTool.e("onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        LogTool.e("onRestart");
    }

    @Override
    protected void onStart() {
        super.onStart();
        LogTool.e("onStart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LogTool.e("onDestroy");
        unbinder.unbind();
        this.setContentView(R.layout.activity_null);
        //System.gc();
    }

    public void btnClisk(View view) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityAnimUtil.startAnim(this);
    }
}
